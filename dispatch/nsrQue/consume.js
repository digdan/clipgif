#!/usr/bin/env nodejs
var srq = require('./index.js');
var q = new srq({
	host:'127.0.0.1',
	port:16379,
	password:'t03l1c3rzz3r',
	database:'gifmunch',
	queue:'video'
},function() {
	q.consume(function(err,reply) {
		if (reply) {
			q.jobInfo(reply,function(err,jobData) {
				console.log(jobData);
				q.close();
				process.exit();
			});
		} else {
			console.log('Nothing to consume');
			q.close();
			process.exit();
		}
	});
});


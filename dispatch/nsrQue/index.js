var redis = require('redis');
var async = require('async');
var debug = require('debug')('srq');
var client;
var namespace='sr';
var ready = false;
var database = '0';

const WAIT='wait';
const WORK='work';
const JOB='job';
const HEAD='srQ';
const TIMEOUT=5; //Blocking timeout
const FROZEN_TIMEOUT=(60*6);
const MAX_RETRIES=3;
const EXPIRE_COMPLETE=(60*60);
const EXPIRE_TOTAL=(60*60*5);

var srq = function(options,cb) {
	if (options.queue) {
		namespace = options.queue;
		delete options.queue;		
	}
	if (options.database) {
		database = options.database;
		delete options.database;
	}
	client = redis.createClient(options);	
	client.on('error',function(err) {
		debug('Redis Client Error : '+err);
		cb(err);
	});
	client.on('connect',function() {
		debug('Connected to redis server');
		client.select(database, function() {
			debug('Selected database '+database);
			cb(null);
		});
	});
};

function generateID(chars) {
	var id = '';
	for (i=0;i<=chars;i++) id += Math.floor(Math.random()*16).toString(16); 
	return id;
}

srq.prototype.getKey = function(type,id) {
	if (type == undefined) type = WAIT;
	if (type == JOB) {
		if (id == undefined) return false;
		return HEAD+":"+namespace+":"+type+":"+id;
	}
	return HEAD+":"+namespace+":"+type;
}

srq.prototype.add = function (data,id,cb) {
	if (typeof id == 'function') {
		cb = id;
		id = undefined;
	}
	if (id == undefined) id = generateID(32);
	var jobKey = this.getKey(JOB,id);
	var waitKey = this.getKey(WAIT);
	async.series([
		function (scb) {
			client.hset(jobKey,'data',JSON.stringify(data),scb);
		},
		function (scb) {
			client.expire(jobKey,EXPIRE_TOTAL,scb);
		},
		function (scb) {
			client.lpush(waitKey,jobKey,scb);
		},
		function (scb) {
			client.publish(waitKey,jobKey,scb);
		}
	], function (err,acb) {
		cb(null,jobKey);
	});
}

srq.prototype.count = function (type,cb) {
	client.llen( this.getKey(type), function(err,reply) {
		cb(null,reply);
	});
}

srq.prototype.jobInfo = function (jobKey,cb) {
	client.hgetall(jobKey,function(err,reply) {
		cb(null,reply);
	});
}

srq.prototype.jobs = function (cb) {
	var out = [];
	var self = this;
	client.lrange(this.getKey(WAIT),0,-1,function(err,reply) {
		if (reply) {
			async.each(reply, function(key,scb) {
				self.jobInfo(key,function(err,reply) {
					out.push(reply);
					scb();
				});
			}, function(err) {
				cb(null,out);
			});
		} else {
			cb(null,out);
		}
	});
}

srq.prototype.consume = function(cb) {
	var self = this;
	this.count(WAIT,function(err,count) {
		if (count > 0) {
			client.brpoplpush(self.getKey(WAIT),self.getKey(WORK),TIMEOUT,function(err,reply) {
				if (reply) {
					debug('Consumed key '+reply);
					client.hset(reply,'_qT', Math.floor( Date.now() / 1000));
					cb(null,reply);
				}
			});
		} else {
			cb(null);
		}
	});
}

srq.prototype.complete = function(jobKey,cb) {
	async.series([	
		function (scb) {
			client.lrem(this.getKey(WAIT),1,jobKey,scb);
		},
		function (scb) {
			client.hset(jobKey,'_c',Math.floor( Date.now() / 1000),scb);
		},
		function (scb) {
			client.expire(jobKey,EXPIRE_COMPLETE,scb);
		}
	], function(err,acb) {
		//TODO callback urls
		cb(null);	
	});
}

srq.prototype.janitor = function(cb) {
	var jobKey='';
	var timeChecked=0;
	var self = this;
	this.count(WORK,function(err,count) {
		if (count > 0) {
			debug('Found items in work queue');
			async.series([
				function(scb) {
					client.lindex(self.getKey(WORK),-1,function(err,reply) {
						jobKey = reply;
						scb();
					});
				},
				function (scb) {
					client.hget(jobKey,'_qT',function(err,reply) {
						timeChecked = reply;
						scb();
					});
				},
				function (scb) {
					var elapse = (Math.floor( Date.now() / 1000) - timeChecked);
					debug('Oldest job running for '+elapse+' seconds. Max is '+FROZEN_TIMEOUT);
					if ( elapse > FROZEN_TIMEOUT) {
						debug('Found expired work job');
						self.returnKey(jobKey, function(err,reply) {
							scb();
						});
					} else {
						debug('No expired jobs');
						scb();
					}
				}
			],function(err,acb) {
				cb();
			});			
		} else {
			debug('No jobs in work queue.');
			cb(null);
		}
	});
}

srq.prototype.returnKey = function (jobKey,cb) {
	var attempts = "0";
	var self = this;
	client.hget(jobKey,'_a', function(err,reply) {
		attempts = parseInt(reply)||0;
		if (attempts >= MAX_RETRIES) {
			debug('Job past max retries');
			async.series([
				function(scb) {
					client.lrem(self.getKey(WORK),1,jobKey,function(err,reply) {
						scb();
					});
				},
				function (scb) {
					client.del(jobKey,function(err,reply) {
						scb();
					});
				}
			], function(err,acb) {
				cb(null);
			});
		} else {
			//move key back
			debug('Job sent back to wait queue');
			client.brpoplpush(self.getKey(WORK),self.getKey(WAIT),TIMEOUT,function(err,reply) {
				if (reply) {
					async.series([
						function(scb) {
							client.hdel(reply,'_qT',function(err,reply) {
								scb();
							});
						},
						function(scb) {
							client.hincrby(reply,'_a',1,function(err,reply) {
								scb();
							});
						}
					],function (err,acb) {
						cb(null);
					});
				} else {
					cb('Janitor: Unable to move key');
				}
			});
		}
	});
}

srq.prototype.close = function() {
	client.quit();
}



module.exports = srq;

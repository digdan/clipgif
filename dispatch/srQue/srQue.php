<?php
	class srQue extends \Predis\Client {

		//Redis Key Namespacing
		var $namespace='sr';
		const WAIT='wait';
		const WORK='work';
		const JOB='job';
		const HEAD='srQ';
		const TIMEOUT=5; //Blocking timeout 
		const FROZEN_TIMEOUT=(60*6); //Working job age until returned to que
		const MAX_RETRIES=3;
		const EXPIRE_COMPLETE=(60*60); //Wait time for completed job data to expire
		const EXPIRE_TOTAL=(60*60*5); //Total time a job will be in any que

		function __construct($args) {
			if ($args['que']) {
				$this->setNamespace($args['que']);
				unset($args['que']);
			}
			return parent::__construct($args);
		}

		function setNamespace($namespace) {
			$this->namespace = $namespace;
		}

		function getKey($type=NULL,$id=NULL) {
			if (is_null($type)) $type = self::WAIT;
			if ($type == self::JOB) {
				if (is_null($id)) return false;
				return self::HEAD.":".$this->namespace.":".$type.":".$id;
			}
			return self::HEAD.":".$this->namespace.":".$type;
		}

		function add($data,$id=NULL) {
			if (is_null($id)) $id = md5(uniqid('',true));
			$jobKey = $this->getKey(self::JOB,$id);
			$waitKey = $this->getKey(self::WAIT);
			$this->hset($jobKey,'data',json_encode($data));
			$this->expire($jobKey, self::EXPIRE_TOTAL );
			$this->lpush($waitKey,$jobKey);
			$this->publish($waitKey,$jobKey); //Tell any subscribers of new job
			return $jobKey;
		}

		function count($type) {
			return $this->llen( $this->getKey( $type ) );
		}

		function jobs() {
			$out = array();
			$keys = $this->lrange($this->getKey(self::WORK),0,-1);
			foreach($keys as $key) {
				$out[] = $this->jobInfo($key);	
			}
			return $out;
		}

		function jobInfo($key) {
			$data = $this->hgetall($key);
			return $data;
		}

		function consume() {
			if ($this->count(self::WAIT) > 0) { //Jobs to be had
				$jobKey = $this->brpoplpush($this->getKey(self::WAIT),$this->getKey(self::WORK),self::TIMEOUT);
				if ($jobKey) { 
					$this->hset($jobKey,'_qT',time()); //stamp job with time it entered work que				
					return $jobKey;
				} else return false;
			}
			return false; //Nothing to consume
		}

		function complete($key) {
			//Mark job as completed from work que
			$this->lrem($this->getKey(self::WAIT),1,$moveKey);
			$this->hset($key,'_c',time());
			$this->expire($key,self::EXPIRE_COMPLETE);
			if ($callback_url = $this->hget($key,'_b')) { //Callback url?
				file_get_contents($callback_url."?id=".$key);
			}
		}

		function janitor() { //Check for expired jobs in work que, return to wait que or giveup -- run only one janitor at a time
			$removed = 0;
			$moved = 0;
			$scanned = 0;
			if ($this->count(self::WORK) > 0) {
				$key = $this->lindex($this->getKey(self::WORK),-1);
				$scanned++;
				$timeCheck = $this->hget($key,'_qT');
				if (time() - $timeCheck > self::FROZEN_TIMEOUT) { //Job was frozen, return to wait que
					$attempts = (int)($this->hget($key,'_a') || 0);
					if ($attempts == self::MAX_RETRIES) { //Give up
						$this->lrem($this->getKey(self::WORK),1,$key);
						$removed++;	
					} else {
						$movedKey = $this->brpoplpush($this->getKey(self::WORK),$this->getKey(self::WAIT),self::TIMEOUT);
						$this->hdel($key,'_qT');
						$this->hincrby($key,'_a',1); //Mark as another attempt
						$moved++;
					}
				}
			}
			return array(
				'scanned'=>$scanned,
				'moved'=>$moved,
				'removed'=>$removed
			);
		}
	}
?>

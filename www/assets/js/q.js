$( document ).ready(function() {

    function checkQ() {
        $.ajax({
            type:"GET",
            dataType:"json",
            url:"/video/q"
        }).done(function( data ) {
            $('#Q').html( data.q );
            $('#myQ').html( data.myq );
        });
    }

    window.setInterval(function() {
        checkQ();
    }, 5000);

    checkQ();
});
<?php
	//Initiate Autoloader
	include("autoload.php");

	//Load Configuration file
	Config::setFile('./config.php');
	$config = Config::getInstance();

	$cache = Cache::getInstance( $config["cache"]["type"], $config["cache"]["host"] );

	//Set template base path
	Scope::setBasePath($config["paths"]["templates"]);

	DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] ); //Define DB Connection, use like : DB::getInstance()->prepare("SELECT * FROM ...

	$router = Router::getInstance();

	//Set Session Cookie Parameters
	session_set_cookie_params( $config["session"]["lifetime"] , "/", $config["session"]["domain"]  );
	session_start();

	Auth::siteKey( $config["siteKey"] ); //Initiate Auth class parameters

	$auth = Auth::getInstance();
	$auth->checkSession();

	Scope::globals( 'title', $config["title"] );
	Scope::globals( 'meta', $config["meta"] );
	Scope::globals( 'auth', $auth );
   	Scope::globals( 'avatar', Auth::avatar(array('user_id'=>$auth->user_id())));
	Scope::globals( 'user_data', clipgif_Auth::user_data());

	clipgif_Preparer::checkQue(); //Eat Que

	Scope::globals( 'q' , controllers_Build::getQ() );

    include('routes.php');

	$route = $router->match();	
	Scope::globals( 'route_name', ((isset($route["name"])) ? $route["name"] : 'undefined') );

	if ($route == FALSE ) {
		$num = Utils::decode($_SERVER["REQUEST_URI"]);
		if (is_numeric($num)) { //Looks like a view URL
			$handle = DB::getInstance()->query("SELECT id FROM clips WHERE id = {$num}");
			if ($handle && $handle->fetch()) {
				$route = array( //Yup it was
					"target"=>"controllers_View#main",
					"params"=>array(
						"id"=>$num
					)
				);
			} else { //Deleted?
				$handle = DB::getInstance()->query("SELECT job_id FROM deleted WHERE clip_id = {$num}");
				if ($res = $handle->fetch()) { //Yup
					DB::getInstance()->query("UPDATE deleted SET hits = hits + 1 WHERE clip_id = {$num}");
					$handle = DB::getInstance()->query("SELECT id FROM clips WHERE job_id = {$res["job_id"]}");
					if ($ret = $handle->fetch()) {
						header("Location: /".Utils::encode($ret["id"]));
						die();
					} else {
						$route = array( //Temp rebuilt
							"target"=>"controllers_View#unavail",
							"params"=>array(
								"id"=>$num
							)
						);
					}
				} else {
					Router::forced404( TRUE );
				}
			}
		}
	}

	try {
		if ( isset($route) ) $router->execute( $route );
		if (Router::forced404()) {
			$router->execute( array( //Assume 404
				"target"=>"controllers_Errors#notFound",
				"params"=>""
			) );
		}
	} catch ( Exception $e ) {
		var_dump($e);
		Scope::getInstance( array( "error" => $e->getMessage() ) )->render('error.php');
	}
?>

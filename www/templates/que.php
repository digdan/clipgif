<?=$header?>
<?
	if (isset($list_title)) {
		echo "<h2>{$list_title}</h2>";
	}
?>
<?php if (count($que) == 0) { ?>

	<I>Que is Empty</I>

<? } else {

	foreach($que as $q) {
		$panel_type = "default";
		$extra = "";
		if ((isset($_SESSION["user_id"])) and ($_SESSION["user_id"] == $q["user_id"])) {
			$panel_type = "info"; //Your Clip
			if ($q["pid"] != 0) {
				$panel_type = "success"; //Your Clip being Processed
				$extra = "Processing";
			}
		} elseif ($q["pid"] != 0) {
			$panel_type = "primary";
			$extra = "Processing";
		}
	?>
		<div id="panel_<?=$q["id"]?>" class="panel panel-<?=$panel_type?>">
			<div class="panel-heading"><B><?=$q["title"]?></B> By <A HREF="<?= Router::getInstance()->generate('by_user',['user_id'=> Utils::encode($q['user_id'])]) ?>"><i class="fa fa-user"></i> <?= $q["display_name"] ?></A> <span data-livestamp="<?=$q["when"]?>"></span> <span class="pull-right"><?= $extra ?></span></div>
			<div class="panel-body">
				<p>GIF : <A HREF="<?=$q["source_animate"]?>" TARGET="_BLANK"><?=$q["source_animate"]?></A></p>
				<p>Audio : <A HREF="<?=$q["source_audio"]?>#t=<?=$q["job_config"]["audioOffset"]?>" TARGET="_BLANK"><?=$q["source_audio"]?></A></p>
			</div>
		</div>

	<? }

 } ?>
 <script>
	 window.setInterval(function() {
		 location.reload()
	 }, 5000);
 </script>

<?=$footer?>
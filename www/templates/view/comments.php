				<ul class="list-group">
					<li class="list-group-item">
						<?php
							if (count($comments) == 0) {
								echo "<I> No Comments </I>";
							} else {
								foreach($comments as $k=>$comment) {
									?>
									<div class="row">
										<div class="col-xs-2 col-md-1">
											<a href="<?= Router::getInstance()->generate('by_user',array('user_id'=>Utils::encode(Auth::user_id())))?>">
												<img src="<?=Auth::avatar(array('user_id'=>$comment['user_id']))?>" class="img-circle img-responsive" alt="" />
												<?=$comment['display_name']?>
											</a>
										</div>
										<div class="col-xs-10 col-md-11 text-left">
											<div>
												<div class="mic-info">
													<small class='disabled pull-right muted'><?= date('r',$comment['created']) ?></small>
												</div>
											</div>
											<div class="comment-text">
												<?= $comment['comment'] ?>
											</div>
										</div>
										<div class="action pull-right">
												<?
													if ($auth->user_id() and (($comment["user_id"] == $auth->user_id()) or ($user['id'] == $auth->user_id())) or ($auth->isAdmin())) { ?>
														<button type="button" class="btn btn-danger btn-xs" title="Delete">
														<span class="glyphicon glyphicon-trash"></span>
														</button> &nbsp; &nbsp;
													<? }
													//Only owner of original video and commenter can delete said comment.
												?>
										</div>
									</div>
									<?
										if ($k != (count($comments) - 1)) {
											echo "<HR>";
										}
								}
							}
						?>

					</li>
				</ul>
				<?
				if (isset($_SESSION["user_id"])) {
					include("comments-add.php");
				} else {
					echo "<a href='#loginModal' data-toggle='modal' class='btn btn-primary'><I>Login to comment</I></a>";
				}
				?>
<BR><BR>

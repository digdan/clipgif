<li class="list-group-item">
    <form method="post">
        <div class="row">
            <div class="col-xs-2 col-md-1">
                <img src="<?=$avatar?>" class="img-circle img-responsive" alt="" />
	            <?=$user_data['display_name'];?>
            </div>
            <div class="col-xs-10 col-md-11">
                <div class="comment-text">
                    <textarea name="comment" style="width:100%"></textarea>
                </div>
                <BR>
                <button class="btn btn-primary pull-right"> Add Comment </button> &nbsp;&nbsp;
            </div>
        </div>
    </form>
</li>
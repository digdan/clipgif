<?=$header?>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- ClipGif Leader -->
<div style='margin:0px auto;width:728px'>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7937169317942721"
     data-ad-slot="2877713783"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>
<div class="row" style="resize:both">
	<div class="text-center" style="margin:0px auto;margin-top:40px;">
		<video id="clip" style="width:100%;max-height:600px;" controls preload="auto" poster="<?=$image?>" class="frontClip">
			<source src="/video/<?=$video_id?>.webm" type="video/webm">
			Your browser does not support the video tag.
		</video>
		<H1 class='title'><?=$job->title?></H1>
	</div>
	<div style="margin:10px auto;width:100%;background-color:#ddd;padding:10px;padding-top:0px;margin-top:0px;text-align:center">

		<span class="btn-group" style="vertical-align:top;width:60px"><button id="btnUp" <?= isset($_SESSION["user_id"]) ? "onClick=\"castVote(this,1);\"" : "data-toggle=\"modal\" href=\"#loginModal\" "?> class="btn3d btn-warning btn-up <?= ($vote == 1) ? 'selected':''?>"> <i class="fa fa-arrow-up fa-2x"></i> </button></span>

			<span id="score" style="padding:20px" class="text-center score <?
			if ($job->score == 0) echo "neutral";
			if ($job->score > 0) echo "positive";
			if ($job->score < 0) echo "negative";
			?>"><?= $job->score ?></span>

		<span class="btn-group" style="width:60px;vertical-align:top"><button id="btnDown" <?= isset($_SESSION["user_id"]) ? "onClick=\"castVote(this,-1);\"" : "data-toggle=\"modal\" href=\"#loginModal\" "?> class="btn3d btn-info btn-down <?= ($vote == -1) ? 'selected':''?>"><i class="fa fa-arrow-down fa-2x"></i></button></span>
	</div>
	<div class="text-center" style="margin:0px auto;max-width:<?= ($width)?>px;">
		<div class="col-md-12">
			Created by  <A HREF="<?= Router::getInstance()->generate('by_user',['user_id'=>$user['id']]) ?>"><?= $user["display_name"] ?></A> <span data-livestamp="<?=$when?>"></span>
			&nbsp;&nbsp;
			Views : <?= $views ?>
		</div>
		<div class="col-md-12">
			BPMS <?=$bpms?>
		</div>
		<div class="col-md-12">
			 GIF <a href="<?=$job->source_animate?>" target="_BLANK"><?=$job->source_animate?></a>
		</div>
		<div class="col-md-12">
			AUDIO <a href="<?=$job->source_audio?>" target="_BLANK"><?=$job->source_audio?></a>
		</div>
		<!-- <?=$job_id?> / <?=$orig_id?> -->
	</div>
</div>

<div class="row">
	<div class="col-md-12 text-center">
		<?php
			include("comments.php");
		?>
	</div>
</div>


<div class="row">
	<div class="col-md-12 text-center">
		<?php if ((Utils::encode($_SESSION["user_id"]) == $user["id"]) && (isset($_SESSION["user_id"]))) {
			include("edit.php");
		} ?>
	</div>
</div>

<?php
    include("proximity.php");
?>


<?php if (@$_SESSION["user_id"]) { ?>
	<script>

		function castVote(who,vote) {
			$('.btn-up,.btn-down').removeClass('selected');
			$(who).addClass('selected');
			$.ajax({
				url: "<?= Router::getInstance()->generate('vote',['user_id'=>Utils::encode($_SESSION["user_id"]),'job_id'=>Utils::encode($job_id)])?>?vote="+vote,
				data: vote,
				dataType: 'json'
			}).done(function(data) {
				console.log(data);
				$('#score').removeClass('neutral').removeClass('positive').removeClass('negative');
				$('#score').addClass(data.class);
				$('#score').text(data.score);
			});
		}

	</script>
<?
}
?>

<?=$footer;?>

<div class="colorgraph"></div>
<div class="row">
<?php
	
	foreach($proximity as $prox) {
		if (is_null($prox)) {
			echo "<DIV class=\"col-md-2\" style=\"max-height:100px;overflow:hidden\">";
			echo "</DIV>";
		} else {
			echo "<DIV class=\"col-md-2 text-center\" style=\"max-height:100px;overflow:hidden\">";
			echo "<A HREF=\"/{$prox}\"><IMG class=\"img-responsive img-thumbnail\" style=\"max-height:100px;\" SRC=\"".Router::getInstance()->generate('thumb',array('id'=>$prox))."\"></A>";
			echo "</DIV>";
		}
	}
?>
</div>
<div class="colorgraph"></div>
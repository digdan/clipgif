<?=$header?>

<div class="row" style="resize:both">
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>

    <div class="col-md-12 text-center">
        <B> Find BPM of a GIF </B>
        <div class="input-group form-group">
            <input class="form-control input-block" type="text" name="url" id="gifURL" value="" placeholder="GIF URL">
            <div class="input-group-btn">
                <button class="btn btn-primary pull-right" onClick="gifBPM()"> Find BPM </button>
            </div>
        </div>
        <div class="col-md-12 text-center" id="message"> </div>
        <HR>
    </div>

    <div class="col-md-6 text-center">
        <table class="table table-striped table-bordered">
            <tr>
                <th>Submitted by</th>
                <td><?=$gif['user_display_name']?></td>
            </tr>
            <tr>
                <th>When</th>
                <td><?=date("m/d/Y",$gif['when']);?></td>
            </tr>

            <tr>
                <th>URL</th>
                <td><A HREF="<?=$gif['original_url']?>" TARGET="_BLANK"><?= strlen($gif['original_url']) > 50 ? substr($gif['original_url'],0,50)."..." : $gif['original_url'];?></td>
            </tr>
            <tr>
                <th>Duration</th>
                <td><?=$gif['duration']?> ms</td>
            </tr>
            <tr>
                <th>Frames</th>
                <td><?=$gif['frames']?></td>
            </tr>
            <tr>
                <th>Width</th>
                <td><?=$gif['width']?> pixels</td>
            </tr>
            <tr>
                <th>Height</th>
                <td><?=$gif['height']?> pixels</td>
            </tr>
            <tr>
                <th>BPMS</th>
                <td><?php
                    $out["bpms"] = "<ul class='list-inline text-center bpms'>";
                    $out["bpms"] .= "<li>";
                    foreach(clipgif_BPMer::findBPM( $gif['duration'] ) as $bpm) {
                        $out["bpms"] .= "<span class='label label-primary'>{$bpm} bpm</span> &nbsp; ";
                        if (($bpm >= 100) and ($bpm < 200)) {
                            $search_bpm = round($bpm);
                        }
                    }
                    echo $out['bpms'];
                    ?></td>
            </tr>
            <tr>
                <th>Resources</th>
                <td>

                    <A class="btn btn-default" HREF="http://jog.fm/workout-songs/at/<?=$search_bpm?>/bpm" TARGET="clipgif"> <i class="fa fa-external-link"></i> Jog.FM </A>
                    <A class="btn btn-default" HREF="http://cs.ubc.ca/~davet/music/bpm/<?=$search_bpm?>.html" TARGET="clipgif"> <i class="fa fa-external-link"></i> Dave Tompkins BPM DB</A>
                </td>
            </tr>
        </table>

        <?php if ( Auth::isAuthed() ) { ?>
            <A class="btn btn-success btn-block btn-lg" HREF="<?= Router::getInstance()->generate("build_page")?>?d=<?=$gif["original_url"]?>"> <i class="fa fa-play"></i> Use in ClipGIF </A>
        <?php } ?>
    </div>
    <div class="col-md-6">
        <img class="buildImage" style="width:100%;background-size:cover;" onClick="$(this).toggleClass('fullscreen');" src="<?=$gif['original_url']?>">
    </div>
</div>

<script>
    function gifBPM() {
	    var input = $('#gifURL').val();
	    if (!isNaN(parseFloat(input)) && isFinite(input)) {
		    window.location.href = '/video/gifs/'+input.toString()+'/1';
	    }
        $.ajax({
            url: "<?= Router::getInstance()->generate('gifify'); ?>",
            type: "POST",
            dataType: "json",
            data: { url: input },
            beforeSend: function(xhr) {
                $("#message").html('<i class="fa fa-spinner fa-spin"></i> <B>Fetching GIF</B>');
            }
        }).done( function(data) {
            if (data.duration_ms > 0) {
                window.location.href = '/pool/'+data.id
                /*
                 $("#bpms").html(data.bpms);
                 $("#preview_img").attr("src",data.url);
                 */
            } else {
                $("#message").html('Error parsing GIF');
            }
        });
    }
</script>


<?=$footer;?>

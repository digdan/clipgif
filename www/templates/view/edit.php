<HR>
<script type="text/javascript" src="/assets/js/bootstrap-tagsinput.js" ></script>
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-tagsinput.css">
<div class='row'>
	<div class='col-md-12'>
		<form method="POST" action="<?= Router::getInstance()->generate('rebuild',['id'=>Utils::encode($job_id)])?>">
			<?
			$job_config = unserialize( $job->config );
			?>
			<div class="panel-group" id="accordian">
				<div class="panel panel-default collapsable" id="panelDetails">
					<div class="panel-heading">
						<h3 class="panel-title text-center">
							<a data-toggle="collapse" data-target="#collapseDetails" href="#collapseDetails">ClipGIF Details</a>
						</h3>
					</div>
					<div id="collapseDetails" class="panel-collapse collapse out">
						<div class="panel-body">
							<div class='form-group row'>
								<div class='col-md-1'>
									<label for="title">Title</label>
								</div>
								<div class='col-md-9'>
									<input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?=$job->title?>"/>
								</div>
								<div class='col-md-2'>
									<label for='nsfw'>Not Safe For Work</label> &nbsp;
									<input id="nsfw" type="checkbox" name="nsfw" class="switch" value="1" <?= (isset($job->nsfw)?'checked':'')?>>
								</div>

							</div>

							<div class='form-group row'>
								<div class='col-md-1'>
									<label for="gifURL">Gif URL</label>
								</div>
								<div class='col-md-8'>
									<input type="text" class="form-control" id="gifURL" placeholder="GIF Url" readonly value="<?=$job_config["animateInfo"]["url"]?>"/>
								</div>
								<div class='col-md-1'>
									<label for="gifloops">Loops</label>
								</div>
								<div class="col-md-2">
									<input type="text" size="3" maxlength="3" class="form-control" id="gifloops" name="GIFLoops" placeholder="Gif Loops" value="<?= $job_config["GIFLoops"] ?>" />
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-1"> <label for="audioURL">Audio URL</label> </div>
								<div class="col-md-8"> <input type="text" class="form-control" id="audioURL" placeholder="Audio Url" readonly value="<?= $job_config["audioInfo"]["url"]?>"/> </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <input style="width:5em" type="number" max="99" min="0" class="form-control" name="youtubeOffset[hour]" id="youtubeOffset[hour]" required="required" placeholder="00" value="<?=$audioParts['hour']?>">
                                        <input style="width:5em" type="number" max="60" min="0" class="form-control" name="youtubeOffset[minute]" id="youtubeOffset[minutes]" required="required" placeholder="00" value="<?=$audioParts['minute']?>">
                                        <input style="width:5em" type="number" max="60" min="0" class="form-control" name="youtubeOffset[second]" id="youtubeOffset[second]" required="required" placeholder="00" value="<?=$audioParts['second']?>">
                                    </div>
                                </div>

							</div>

							<div class="form-group row">
								<div class="col-md-1"> <label for="tags">Tags</label> </div>
								<div class="col-md-11"> <input type="text" name="tags" class="form-control pull-left" id="tags" placeholder="Add Tags" value="<?= $tags ?>"/> </div>
							</div>

							<div class='col-md-6 col-md-offset-3'>
								<div class="btn-group">
									<button type="button" class="btn btn-success btn-lg" onClick="submit();">Rebuild Clip</button>
									<button type="button" class="btn btn-success dropdown-toggle btn-lg" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu" role="menu">
										<li><A HREF="#deleteConfirmOpen" data-toggle="collapse" class="label-danger"><i class="fa fa-times"></i> Delete Clip</A></li>
									</ul>
								</div>
								<div id="deleteConfirmOpen" class="collapse">
									<p>Are you sure?</p>
									<A HREF="<?= Router::getInstance()->generate('video_delete',['id'=>$video_id])?>" class='btn btn-danger'>Yes</A> / <A HREF="#deleteConfirmOpen" data-toggle="collapse" class='btn btn-primary'>No</A>
								</div>
								<BR><BR><BR>
							</div>

						</div>
					</div>
				</div>
                <BR>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#tags').tagsinput({
		  maxTags: 5, confirmKeys: [13,32,44]
		});
	});
</script>
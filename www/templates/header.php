<!DOCTYPE html>
<html lang="en">
	<head>
		<?
			foreach($meta as $mk=>$mv) {
				if (is_array($mv)) {
					echo "<meta ";
					foreach($mv as $mmk=>$mmv) {
						echo "{$mmk}=\"{$mmv}\" ";
					}
					echo " />\n";
				} else {
					echo "<meta name=\"{$mk}\" content=\"{$mv}\">\n";
				}
			}
		?>

		<title><?= $title ?></title>

		<script type="text/javascript" src="/assets/js/jquery.min.js" ></script>
		<script type="text/javascript" src="/assets/js/moment.js" ></script>
		<script type="text/javascript" src="/assets/js/livestamp.js" ></script>
		<script type="text/javascript" src="/assets/js/custom.js" ></script>

		<link href='http://fonts.googleapis.com/css?family=Candal' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-theme.min.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-yeti.css">

        <link rel="stylesheet" type="text/css" href="/assets/css/custom.css">
	</head>
	<body>

		<nav class="navbar navbar-fixed-top navbar-default" id="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/">ClipGif</a>
				</div>
				<div class="collapse navbar-collapse navbar-ex1-collapse">
					<ul class="nav navbar-nav">
						<li <?= ($route_name == 'recent') ? "class='active'":''?> ><a href="<?= Router::getInstance()->generate('recent')?>">New</a></li>
						<li <?= ($route_name == 'top_ranked' || $route_name == 'ranked') ? "class='active'":''?>><a href="<?= Router::getInstance()->generate('top_ranked')?>">Ranked</a></li>
						<li <?= ($route_name == 'top_viewed' || $route_name == 'viewed') ? "class='active'":''?>><a href="<?= Router::getInstance()->generate('top_viewed')?>">Viewed</a></li>
						<li <?= ($route_name == 'gifpool' || $route_name == 'gifpool_page') ? "class='active'":''?>><a href="<?= Router::getInstance()->generate('gifpool')?>">GIF Pool</a></li>
						<li <?= ($route_name == 'forum' || $route_name == 'forum_page') ? "class='active'":''?>><a href="<?= Router::getInstance()->generate('forum')?>">Forum</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<?php if ( Auth::isAuthed() ) { ?>
							<li>
								<button onClick="location.href='<?= Router::getInstance()->generate('build')?>'" class="btn navbar-btn btn-success">Build a ClipGIF</button>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$user_data['display_name']?> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="<?= Router::getInstance()->generate('by_user',array('user_id'=>Utils::encode(Auth::user_id())))?>">Your ClipGIFs</a></li>
									<li><a href="#">Edit Profile</a></li>
									<li><a href="#">Edit Account</a></li>
									<li class="divider"></li>
									<li><a href="<?= Router::getInstance()->generate('logout')?>">Logout</a></li>
								</ul>
							</li>

							<li>
								<a href="<?= Router::getInstance()->generate('top_que')?>">
									<span class="badge pull-right"> <span id="Q"><?= $q['q'] ?></span> / <span id="myQ"><?= $q['myq'] ?></span> </span>
									Que &nbsp;
								</a>
							</li>

						<?php } else { ?>
							<li>
								<button href="#loginModal" data-toggle="modal" class="btn navbar-btn btn-success">Build a ClipGIF</button>
							</li>
							<li><a href="#loginModal" data-toggle="modal">Login</a></li>
							<li><a href="<?= Router::getInstance()->generate('register')?>">Register</a></li>
						<?php } ?>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav>
		<div class="container content">
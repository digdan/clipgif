<?=$header?>
	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<? if (($route_name == "front") and ($page_current == 1)) include('templates/front.php'); ?>
		<?php
			if (isset($list_title)) {
				echo "<h2>{$list_title}</h2>";

			}
		?>
	<div class="text-center">
		<?=$paginate?>
	</div>
	<div class='cellWrapper center-block text-center' style="margin:0 auto;">
		<?php
			$count = 0;
			foreach($clips as $clip) {
				if ($clip['id'] == 'ad') { ?>
				<div class="cell">
					<div class="frontClip ad">
						<div class='frontCover'>
							<!-- ClipGif Inline2 -->
							<ins class="adsbygoogle"
							     style="display:inline-block;width:300px;height:250px"
							     data-ad-client="ca-pub-7937169317942721"
							     data-ad-slot="4781799381"></ins>
							<script>
								(adsbygoogle = window.adsbygoogle || []).push({});
							</script>
						</div>
					</div>
				</div>
				<?php } else { ?>
					<div class="cell">
						<!--
					<?php var_dump($clip); ?>
				-->
						<div class="frontClip">
							<A HREF="/<?= $clip["id"] ?>">

								<? if (strstr($clip["data"]["tags"], "nsfw")) { ?>
									<div class='frontCover'
									     style="background-image:url( http://placehold.it/200x200&text=NSFW )"></div>
									<span class='nsfw'>NSFW</span>
								<? } else { ?>
									<div class='frontCover'
									     style="background-image:url( <?= Router::getInstance()->generate('thumb', ['id' => $clip["id"]]) ?> )"></div>
								<? } ?>
								<span class="title"><?= $clip["data"]["title"] ?></span>
							</A> <BR>

							<div style="padding:2px">
								<span class="pull-left" data-livestamp="<?= $clip["data"]["when"] ?>"></span>
								<span class="pull-right"><?= $clip["data"]["views"] ?> Views&nbsp;</span>
							</div>

							<div class="infoBar">

								<A HREF="<?= Router::getInstance()->generate('by_user', ['user_id' => Utils::encode($clip['data']['user_id'])]) ?>"><i
										class="fa fa-user"></i> <?= $clip["data"]["user_display_name"] ?></A>
							<span class="badge pull-right text-info">
								<?php
								if ($clip["data"]["ascore"] == 0) echo "<span class='neutral'>";
								if ($clip["data"]["ascore"] > 0) echo "<span class='positive'>+";
								if ($clip["data"]["ascore"] < 0) echo "<span class='negative'>";
								echo $clip["data"]["ascore"];
								echo "</span>";
								?> &nbsp; <i class='fa fa-thumbs-up'></i>
							</span>
							<span class="badge pull-right" style="margin-right:3px">
								<?
								if ($clip["data"]["comments"] == 0) {
									echo "<span>";
								} else {
									echo "<span class='comments-yes'>";
								}
								?>
								<?= $clip["data"]["comments"]; ?></span>&nbsp; <i class='fa fa-comments-o'></i>
								</span>
							</div>
						</div>
					</div>
				<?php
				}
			}
		?>
	</div>
	<div class="col-xs-12 text-center">
		<?=$paginate?>
	</div>
<?= $footer?>

<?=$header?>
<div class="container">
    <h1>Thank you</h1>
    <p>A validation email has been sent to '<?=$email?>'</p>
    <p>Please check your email and following the directions to activate your account.</p>
</div>
<?=$footer?>
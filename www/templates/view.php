<?=$header?>


	<div class="row" style="resize:both">
		<div class="text-center" style="margin:0px auto;max-width:<?= ($width)?>px;margin-top:0px">
			<video id="clip" style="width:100%" controls preload="none" poster="<?=$image?>" class="frontClip">
			  <source src="/video/<?=$video_id?>.webm" type="video/webm">
			Your browser does not support the video tag.
			</video>
			<H1 class='title'><?=$job->title?></H1>
			<P>
				Created by  <A HREF="<?= Router::getInstance()->generate('by_user',['user_id'=>$user['id']]) ?>"><?= $user["display_name"] ?></A> <span data-livestamp="<?=$when?>"></span>
				&nbsp;&nbsp;
				Views : <?= $views ?>
			</P>

		</div>

<div style='margin:0px auto;width:728px'>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- ClipGif Leader -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-7937169317942721"
     data-ad-slot="2877713783"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

		<div style="margin:0px auto;width:360px;text-align:center">

			<span class="btn-group" style="vertical-align:top;width:60px"><button id="btnUp" <?= isset($_SESSION["user_id"]) ? "onClick=\"castVote(this,1);\"" : "data-toggle=\"modal\" href=\"#loginModal\" "?> class="btn3d btn-warning btn-up <?= ($vote == 1) ? 'selected':''?>"> <i class="fa fa-arrow-up fa-2x"></i> </button></span>

			<span id="score" style="padding:20px" class="text-center score
							<?
					if ($job->score == 0) echo "neutral";
					if ($job->score > 0) echo "positive";
					if ($job->score < 0) echo "negative";
					?>
							"><?= $job->score ?></span>

			<span class="btn-group" style="width:60px;vertical-align:top"><button id="btnDown" <?= isset($_SESSION["user_id"]) ? "onClick=\"castVote(this,-1);\"" : "data-toggle=\"modal\" href=\"#loginModal\" "?> class="btn3d btn-info btn-down <?= ($vote == -1) ? 'selected':''?>"><i class="fa fa-arrow-down fa-2x"></i></button></span>
		</div>
		<BR><BR>
	</div>

	<div class="row">
		<div class="col-md-12 text-center">
            <?php if (Utils::encode($_SESSION["user_id"]) == $user["id"]) { ?>

            	<div class='row'>
            		<div class='col-md-12'>
            			<form method="POST" action="<?= Router::getInstance()->generate('rebuild',['id'=>Utils::encode($job_id)])?>">
				            <?
				                $job_config = unserialize( $job->config );
				            ?>
							<div class="panel-group" id="accordian">
								<div class="panel panel-default collapsable" id="panelDetails">
									<div class="panel-heading">
										<h3 class="panel-title text-center">
											<a data-toggle="collapse" data-target="#collapseDetails" href="#collapseDetails">ClipGIF Details</a>
										</h3>
									</div>
									<div id="collapseDetails" class="panel-collapse collapse">
										<div class="panel-body">
											<div class='form-group row'>
												<div class='col-md-1'>
													<label for="title">Title</label>
												</div>
												<div class='col-md-9'>
													<input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?=$job->title?>"/>
												</div>
												<div class='col-md-2'>
													<label for='nsfw'>Not Safe For Work</label> &nbsp;
													<input id="nsfw" type="checkbox" name="tags[nsfw]" class="switch" value="1" <?= (isset($tags['nsfw'])?'checked':'')?>>
												</div>

											</div>

											<div class='form-group row'>
												<div class='col-md-1'>
													<label for="gifURL">Gif URL</label>
												</div>
												<div class='col-md-8'>
													<input type="text" class="form-control" id="gifURL" placeholder="GIF Url" readonly value="<?=$job_config["animateInfo"]["url"]?>"/>
												</div>
												<div class='col-md-1'>
													<label for="gifloops">Gif Loops</label>
												</div>
												<div class="col-md-2">
													<input type="text" size="3" maxlength="3" class="form-control" id="gifloops" name="GIFLoops" placeholder="Gif Loops" value="<?= $job_config["GIFLoops"] ?>" />
												</div>
											</div>

											<div class="form-group row">
												<div class="col-md-1"> <label for="audioURL">Audio URL</label> </div>
												<div class="col-md-8"> <input type="text" class="form-control" id="audioURL" placeholder="Audio Url" readonly value="<?= $job_config["audioInfo"]["url"]?>"/> </div>
												<div class='col-md-1'>
													<label for="">Audio Offset</label>
												</div>
												<div class="col-md-2">
													<input type="number" class="form-control" id="audioOffset" name="audioOffset" placeholder="audio Offset" required value="<?= $job_config["audioOffset"]?>"/>
												</div>
											</div>

											<div class='col-md-6 col-md-offset-3'>
												<div class="btn-group">
													<button type="button" class="btn btn-success btn-lg" onClick="submit();">Rebuild Clip</button>
													<button type="button" class="btn btn-success dropdown-toggle btn-lg" data-toggle="dropdown">
														<span class="caret"></span>
														<span class="sr-only">Toggle Dropdown</span>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li><A HREF="#deleteConfirmOpen" data-toggle="collapse" class="text-danger"><i class="fa fa-times"></i> Delete Clip</A></li>
													</ul>
												</div>
												<div id="deleteConfirmOpen" class="collapse">
													<p>Are you sure?</p>
													<A HREF="<?= Router::getInstance()->generate('video_delete',['id'=>$video_id])?>" class='btn btn-danger'>Yes</A> / <A HREF="#deleteConfirmOpen" data-toggle="collapse" class='btn btn-primary'>No</A>
												</div>
												<BR><BR><BR>
											</div>

										</div>
									</div>
								</div>
							</div>
            			</form>
            		</div>
				</div>
            <?php } ?>
			<!-- <?=$job_id?> / <?=$orig_id?> -->
		</div>
	</div>

<?php if (@$_SESSION["user_id"]) { ?>
<script>

	function castVote(who,vote) {
		$('.btn-up,.btn-down').removeClass('selected');
		$(who).addClass('selected');
		$.ajax({
			url: "<?= Router::getInstance()->generate('vote',['user_id'=>Utils::encode($_SESSION["user_id"]),'job_id'=>Utils::encode($job_id)])?>?vote="+vote,
			data: vote,
			dataType: 'json'
		}).done(function(data) {
			console.log(data);
			$('#score').removeClass('neutral').removeClass('positive').removeClass('negative');
			$('#score').addClass(data.class);
			$('#score').text(data.score);
		});
	}

</script>
<?
}
 ?>


<?=$footer;?>

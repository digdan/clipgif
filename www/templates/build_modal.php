<div class="modal fade" id="buildModal" tabindex="-1" role="dialog" aria-labelledby="buildModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title"> <i class="fa fa-spin fa-spinner"></i> ClipGIF Que'd</h4>
			</div>
			<div class="modal-body">
				<H2>Your ClipGIF has been placed in the que</H2>
			</div>
            <div id="errors"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Okay</button>
			</div>
		</div>
	</div>
</div>
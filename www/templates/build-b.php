<?=$header?>
	<div class="container">
		<div class="row">
			<div class="col-md-6 ">
				<fieldset>
					<legend>GIF Controls</legend>
					<form method="POST" action="<?= Router::getInstance()->generate('build_animate')?>" id="animate-form">
						<div class="form-group">
							<label for="name">Gif URL</label>
							<input type="text" class="form-control" name="gifURL" id="gifURL" required="required" placeholder="URL to animated GIF" value="">
						</div>
						<input class="btn btn-primary pull-right" type="button" name="cmd" value="Load">
						<div id="errors"></div>
                    </form>
                </fieldset>
            </div>
            <div class="col-md-6">
                <fieldset>
					<legend>Animated GIF</legend>
					<div id="animate"></div>
                </fieldset>
            </div>
		</div>

		<div class="row collapse">
			<div class="col-md-6">
				<form method="POST" action="<?= Router::getInstance()->generate('build_audio')?>" id="audio-form">
					<div class="form-group">
						<label for="name">URL to Youtube Link (for audio)</label>
						<input type="text" class="form-control" name="youtubeURL" id="youtubeURL" required="required" placeholder="URL to Youtube Video" value="">
					</div>
				</form>
			</div>
			<div class="col-md-6">
				<div id="audio"></div>
			</div>
		</div>
	</div>
</div>

	<script>
		var job_id;
		var interval;

		$( document ).ready(function() {
			$('#animate-form').ajaxForm({
				dataType:  'json',
				beforeSubmit : function() {
					$('.animate-forms #errors').html('');
				},
				success: buildAnimate
			});
		});

		function buildAnimate( data ) {
			$('#animage').html( data.html );
		}

		function buildClip(data) {
			console.log(data);
			if (data.job_id == 0) {
				$('.build-forms #errors').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">&times;</a><strong>'+data.error+'</strong></div>');
				$('#submitBtn').html('Create ClipGif');
			} else if (data.job_id == -1) {
				window.location.href = "/"+data.error;
			} else {
				job_id = data.job_id;
				$('.build-forms #errors').html('');
				$('#submitBtn').html('Create ClipGif');
				$('#buildModal').modal( { keyboard:false } );
				$('#buildModal').modal( 'show' );
				interval = window.setInterval( updateProgress , <?= $config["work"]["refresh_rate"]; ?> );
				$('#buildModal').on('hidden.bs.modal', function() {
					window.clearInterval(interval);
				});
			}
		}

		function updateProgress() {
			$.ajax({
				beforeSubmit: function() {
					$('#buildModal #errors').html(' ');
					$('.progress-bar').width('0%');
					$('.modalStatus').html(' ');
				},
				url:'/video/build/'+job_id,
				dataType:'json',
				success: function(data) {

					if (data.id == 'complete') {
						$('#buildModal .progress').removeClass('active');
						window.clearInterval(interval);
						window.location.href = "/"+data.m;
					}

					$('#buildModal .progress').removeClass('active');

					if (data.id == 'error') {
						$('.progress-bar').width('0%');
						$('#buildModal #errors').html('<div class="alert alert-danger"><strong>'+data.error.m+'</strong></div>');
						window.clearInterval(interval);
					}

					$('#buildModal #modal-'+data.id+' .progress').addClass('active');

					$('#buildModal #modal-video .progress-bar').width( data.video.p + "%" );
					$('#buildModal #modal-video .modalStatus').html( data.video.m );

					$('#buildModal #modal-audio .progress-bar').width( data.audio.p + "%" );
					$('#buildModal #modal-audio .modalStatus').html( data.audio.m );

					$('#buildModal #modal-mash .progress-bar').width( data.mash.p + "%" );
					$('#buildModal #modal-mash .modalStatus').html( data.mash.m );

				}
			});
		}

	</script>
<?=$footer?>
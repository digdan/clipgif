<?=$header?>
	<script type="text/javascript" src="/assets/js/bootstrap-tagsinput.js" ></script>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-tagsinput.css">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Build-a-clip
						</h3>
					</div>
					<div class="panel-body">
						<form method="POST" action="<?= Router::getInstance()->generate('build')?>" id="build-form">
                            <fieldset class="show" id="step_video">
                                <legend>Video</legend>

                                <div class="form-group">
                                    <label for="name">URL to Gif</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="gifURL" id="gifURL" required="required" onChange="gifBPM()" placeholder="GIF File URL" value="<?=$defaultURL?>">
                                        <span class="input-group-btn">
                                            <button class='btn input-control btn-default' onClick="return false;"> Fetch </button>
                                        </span>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="form-group">
                                            <label for="name">Loop GIF</label>
                                            <select name="GIFLoops">
                                                <?php
                                                $preselect = 1;
                                                for($i=1;$i<=$config["work"]["loop_max"];$i++) {
                                                    echo "<option value=\"{$i}\" ";
                                                    if ($i == $preselect) {
                                                        echo " SELECTED ";
                                                    }
                                                    echo ">{$i} times</option>\n";
                                                }
                                                ?>
                                                <option value="-1">Max</option>
                                            </select> times.
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <fieldset class="hide" id="step_audio">
                                <legend>Audio</legend>

                                <div class="form-group">
                                    <label for="name">URL to Youtube Link</label>
                                    <input type="text" class="form-control" name="youtubeURL" id="youtubeURL" onChange="showSettings();" required="required" placeholder="Youtube video url" value="">
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-md-offset-6">
                                        <div class="form-group">
                                            <label>Offset</label>
                                            <div class="input-group">
                                                <input onChange='ytPreview();' style="width:5em" type="number" min="0" class="form-control" name="youtubeOffset[hour]" id="youtubeOffset[hour]" placeholder="hh">
                                                <input onChange='ytPreview();' style="width:5em" type="number" min="0" class="form-control" name="youtubeOffset[minute]" id="youtubeOffset[minutes]" placeholder="mm">
                                                <input onChange='ytPreview();' style="width:5em" type="number" min="0" class="form-control" name="youtubeOffset[second]" id="youtubeOffset[second]" required="required" value="00">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

							<div style="margin:0 auto" id="yt_preview"></div>

                            <fieldset class="hide" id="step_settings">
                                <legend>Settings</legend>

                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" class="form-control" name="title" id="title" required="required" placeholder="Title of ClipGIF">
                                </div>

                                <BR><BR>

                                <div class="form-group">
                                    <label for="tags">Tags</label>
                                    <input type="text" name="tags" class="form-control pull-left" id="tags" placeholder="Add Tags" value="<?= $job_config["tags"]?>"/>
                                </div>

                                <div class="form-group pull-right">
                                    <label><input type="checkbox" name="nsfw" value="1"> 18+? </label>
                                </div>

                                <div class="form-group">
                                    <button id="submitBtn" class="btn pull-right btn-success  btn-block btn-lg">Create ClipGif</button>
                                </div>
                                <BR><BR>
                            </fieldset>
                            <div class="form-group" id="errors">
                                <?php
                                if (isset($_GET["s"])) {
                                    echo '<div class="alert alert-success"><a class="close" data-dismiss="alert">&times;</a><strong> ClipGIF placed in que </strong></div>';
                                }
                                ?>
                            </div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6" id="preview" style="height:100%">
				<div class="panel panel-default" style="height:100%">
					<div class="panel-heading">
						<h3 class="panel-title">
							Gif Preview
						</h3>
					</div>
                    <BR><BR>
					<div id="preview_img" class="buildImage" onClick="$(this).toggleClass('fullscreen')" style="background-image:url(http://www.placehold.it/300&text=GIF)" /></div>
					<div id="bpms" style="line-height:60px" class="text-center"></div>
				</div>
			</div>
		</div>



	<script>
		$( document ).ready(function() {
			$('#build-form').ajaxForm({
				dataType:  'json',
				beforeSubmit : function() {
					$('.build-forms #errors').html('');
					$('#submitBtn').html('<i class="fa fa-spin fa-spinner"></i>');
				},
				success: buildClip
			});
			<?= (! is_null($defaultURL)) ? 'gifBPM();' : '' ?>
			$('#tags').tagsinput({
			  maxTags: 5 , confirmKeys: [13,32,44]
			});
		});

		function parseQuery(qstr) {
			var query = {};
			var a = qstr.split('&');
			for (var i in a) {
				var b = a[i].split('=');
				var rep = decodeURIComponent(b[0]).replace(/\?/g,'');
				query[rep] = decodeURIComponent(b[1]);
			}
			return query;
		}

		function buildClip(data) {
		    console.log(data);
			if (data.job_id == 0) {
				$('#build-form #errors').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">&times;</a><strong>'+data.error+'</strong></div>');
				$('#submitBtn').html('Create ClipGif');
			} else if (data.job_id == -1) {
				window.location.href = "/"+data.error;
			} else {
				window.location.href = '<?= Router::getInstance()->generate('build_page')?>?s=1';
			}
		}

		function ytPreview() {
			var parser = document.createElement('a');
			parser.href = $('#youtubeURL').val();
			var varsQuery = parseQuery(parser.search);
			var ts = 0;
			if (!isNaN($('#youtubeOffset\\[second\\]').val() )) ts = ts + ($('#youtubeOffset\\[second\\]').val());
			if (!isNaN($('#youtubeOffset\\[minute\\]').val() )) ts = ts + ($('#youtubeOffset\\[minute\\]').val() * 60);
			if (!isNaN($('#youtubeOffset\\[hour\\]').val() )) ts = ts + ($('#youtubeOffset\\[hour\\]').val() * 60 * 60);
			$('#yt_preview').html('<iframe width="100%" height="35" src="http://www.youtube.com/embed/'+varsQuery['v']+'?rel=0&autohide=0&start='+ts+'#8243;" frameborder="0"></iframe>');
		}

        function showSettings() {
	        //Spawn Youtube preview bar
	        ytPreview();
            //$('#step_settings').removeClass('hide').addClass('show');
        }

		function gifBPM() {
			$.ajax({
				url: "<?= Router::getInstance()->generate('gifify'); ?>",
				type: "POST",
				dataType: "json",
				data: { url: $('#gifURL').val() },
				beforeSend: function(xhr) {
					$("#bpms").html('<i class="fa fa-spinner fa-spin"></i> <B>Fetching GIF</B>');
				}
			}).done( function(data) {
                if (data.duration_ms > 0) {
                    $('#step_audio').removeClass('hide').addClass('show');
                    $('#step_settings').removeClass('hide').addClass('show');
                    $("#bpms").html(data.bpms);
                    $("#preview_img").css("background-image","url("+data.url+")");
                    $("#preview_img").css("height",data.height+"px");
                } else {
                    $('#build-form #errors').html('<div class="alert alert-danger"><a class="close" data-dismiss="alert">&times;</a><strong>Error parsing GIF</strong></div>');
                    $("#bpms").html('Error parsing GIF');
                }
			});

		}
	</script>
<?=$footer?>
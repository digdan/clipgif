<?=$header?>

		<div class="col-md-6">
			<H1>Gif Pool</H1>
			<B> Find BPM of a GIF </B>
			<div class="input-group form-group">
				<input class="form-control input-block" type="text" name="url" id="gifURL" value="<?=$_REQUEST['u']?>" placeholder="GIF URL">
				<div class="input-group-btn">
					<button class="btn btn-primary pull-right" onClick="gifBPM()"> Find BPM </button>
				</div>
			</div>
			<B> Find GIFs of BPM </B>
			<form action="<?= Router::getInstance()->generate("gifpool_find");?>" method="POST">
				<div class="input-group form-group">
					<input class="form-control input-block" type="text" name="bpm" id="bpm" value="<?=@$bpm?>" placeholder="BPMS">
					<div class="input-group-btn">
						<button class="btn btn-primary pull-right"> Find GIFs </button>
					</div>
				</div>
			</form>
		</div>


		<div class="col-md-6 text-center" style="color:black;background-image:url('/assets/images/pool.jpg');background-repeat:no-repeat;background-size:contain;background-position:center center">
			<div style="background:rgba(255,255,255,0.7)">
				<H2>Welcome to the</H2>
				<H1>GIF Pool</H1>
				<P>You can find BPMs of a GIF, or find GIFs of a BPM</P>
			</div>
		</div>



			<HR>
			<div class='clearfix'></div>
			<?php
				if (isset($_POST['bpm'])){
					echo "<H2> GIFs with {$_POST['bpm']}-ish BPM</H2>";
				}
			$count = 0;
			foreach($gifs as $gif) {
			?>
					<div class="cell">
						<div class="frontClip">
								<A HREF="<?= Router::getInstance()->generate("gifPoolSingle",array("id"=>$gif["id"]))?>">
								<div class='frontCover' style="background-image:url(<?= $gif["data"]["original_url"]?>);background-size:cover">
								</div>
							</A>
                                    <div class="infoBar text-center">
                                        <?php if ( Auth::isAuthed() ) { ?>
                                            <A class="btn btn-block btn-success" HREF="<?= Router::getInstance()->generate("build_page")?>?d=<?=$gif["data"]["original_url"]?>"> <i class="fa fa-play"></i> Use in ClipGIF </A>
                                        <?php } ?>

                                        <span class="pull-right">
									<BR>
									<ul class='list-inline text-center'>
										<li>
										<?php
											foreach(clipgif_BPMer::findBPM( $gif["data"]["duration"] ) as $bpm) {
												echo "<span class='label label-primary'>{$bpm} bpm</span> &nbsp; ";
											}
										?>
										</li>
										<li><span class='label label-info'> <?= round($gif["data"]["duration"]) ?> ms</label></li>
										<li><span class='label label-default'> <?= $gif["data"]["frames"] ?> frames</label></li>
									</ul>
								</span>
							</div>
						</div>
					</div>

				<?php
				$count++;
			}
			?>

		<div class='clearfix'>
			<BR><BR>
		</div>

<BR><BR>

		<div class="col-xs-12 text-center">
			<?=$paginate?>
		</div>

<div class="modal fade" id="bpmModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">GIF BPM Finder</h4>
			</div>
			<div class="modal-body text-center">

				<img id="preview_img" style="width:100%">
				<div id="bpms" class="text-center">
				</div>

			</div>
		</div>
	</div>
</div>



<script>
	$(document).ready(function() {

		function gifBPM() {
			$.ajax({
				url: "<?= Router::getInstance()->generate('gifify'); ?>",
				type: "POST",
				dataType: "json",
				data: { url: $('#gifURL').val() },
				beforeSend: function(xhr) {
					$("#bpms").html('<i class="fa fa-spinner fa-spin"></i> <B>Fetching GIF</B>');
					$('#bpmModal').modal({show:true});
				}
			}).done( function(data) {
				console.log(data);
				if (data.duration_ms > 0) {
					window.location.href = '/pool/'+data.id
					/*
					 $("#bpms").html(data.bpms);
					 $("#preview_img").attr("src",data.url);
					 */
				} else {
					$("#bpms").html('Error parsing GIF');
				}
			});
		}

		<?= ($_REQUEST['u'] ? "gifBPM();\n" : "")?>
	});
</script>


<?=$footer?>
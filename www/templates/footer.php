		</div><!-- /.container -->

		<?php
			if ( ! $auth->isAuthed() ) {
				echo Scope::render('login_modal.php',false);
			} else {
				echo "<script type=\"text/javascript\" src=\"/assets/js/q.js\" ></script>";
			}
		?>

			<footer>
				<div class="col-lg-12 navbar navbar-default">
					<div class="container">
						<p>Copyright &copy; ClipGif.com <?= date('Y'); ?></p>
					</div>
				</div>
			</footer>


		<!-- JavaScript -->
		<script type="text/javascript" src="/assets/js/bootstrap.min.js" ></script>
		<script type="text/javascript" src="/assets/js/jquery.form.js" ></script>

		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-777209-28', 'clipgif.com');
			ga('send', 'pageview');

		</script>
	</body>
</html>
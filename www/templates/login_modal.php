	<div class="modal fade" id="loginModal" role="dialog">
		<form role="form" method="POST" action="<?= Router::getInstance()->generate('login')?>" id="loginForm">
			<div class="container">
				<div class="loginContainer">
					<div class="panel panel-primary panel-inverse">
						<div class="panel-heading">
							<h3 class="panel-title">
								Log in</h3>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-4 col-sm-4 col-md-4 separator social-login-box"> <br />
									<img src="/assets/images/logo-254.png" width="170" class="rounded">
								</div>
								<div class="col-xs-8 col-sm-8 col-md-8 login-box">
									<div class="input-group">
										<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
										<input name="email" type="text" class="form-control" placeholder="Email" required autofocus />
									</div>
									<div class="input-group">
										<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
										<input name="password" type="password" class="form-control" placeholder="Password" required />
									</div>
									<div class="col-xs-12 text-right">
										<span class="button-checkbox">
											<button type="button" class="btn" data-color="info">Remember</button>
											<input type="checkbox" name="remember" id="remember" class="hidden" value="1" <?= (isset($_COOKIE['remember']) || isset($_POST['remember'])) ? 'checked':''?>>
										</span>
										<button type="submit" class="btn btn-labeled btn-success">
											<span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span> Login </button>
										<button type="button" class="btn btn-labeled btn-danger" data-dismiss="modal">
											<span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span> Cancel</button>
									</div>
									<BR><BR>
									<div id="reason"></div>
									<HR>
									<div>
										Don't have an account? <a href="<?= Router::getInstance()->generate('register')?>">Sign up here</a>
									</div>
										<HR>
									<div class="pull-right">
										<a href="<?= Router::getInstance()->generate('lost_password')?>">Lost your password?</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		$( document ).ready(function() {
			$("#loginForm").ajaxForm({
				dataType: 'json',
				success: function(data) {
					if (data.error) {
						$("#loginForm #reason").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button>" + data.error + "</div>");
					} else if (data.href) {
						window.location.href=data.href;
					} else if (data.ok) {
						location.reload();
					}
				},
				error: function(data) {
					$("#loginForm #reason").html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>&times;</button> Error authenticating </div>");
				}
			});
		});
	</script>
<?php
$router->
map("GET","/cron", "controllers_Cron#main","cron")->
map("GET","/" , "controllers_Lists#front",'front')->

map("GET|POST" , "/browse/recent", "controllers_Lists#recent",'recent')->
map("GET|POST" , "/browse/recent/[*:page]", "controllers_Lists#recent",'recent_paged')->
map("GET|POST" , "/browse/user/[*:user_id]", "controllers_Lists#user",'by_user')->
map("GET|POST" , "/browse/user/[*:user_id]/[*:page]", "controllers_Lists#user")->
map("GET|POST" , "/browse/viewed", "controllers_Lists#viewed",'top_viewed')->
map("GET|POST" , "/browse/viewed/[*:page]", "controllers_Lists#viewed",'viewed')->
map("GET|POST" , "/browse/ranked", "controllers_Lists#ranked",'top_ranked')->
map("GET|POST" , "/browse/ranked/[*:page]", "controllers_Lists#ranked",'ranked')->
map("GET|POST" , "/browse", "controllers_Browse#main",'browse')->

map("GET|POST" , "/browse/que","controllers_Lists#que",'top_que')->
map("GET|POST" , "/browse/que/[*:page]","controllers_Lists#que",'que')->

map("GET|POST" , "/other/contact", "controllers_Contact#main",'contact')->
map("GET", "/video/[*:id].".$config["work"]["type"], "controllers_View#video",'video')->
map("GET", "/video/[*:id].".$config["work"]["type_flash"], "controllers_View#flash",'video_flash')->
map("GET", "/video/[*:id].mp4","controllers_View#mp4",'video_mp4')->

map("GET" , "/video/build", "controllers_Build#page",'build_page')->
map("POST" , "/video/build", "controllers_Build#main",'build')->
map("GET|POST" , "/video/rebuild/[*:id]", "controllers_Build#rebuild",'rebuild')->
map("GET",	"/video/q", "controllers_Build#q")->

map("GET|POST" , "/video/gifify","controllers_GIFPool#gifify","gifify")->
map("GET|POST","/pool/[*:id]","controllers_GIFPool#single","gifPoolSingle")->

map("GET|POST", "/video/vote/[*:job_id]/[*:user_id]", "controllers_View#vote",'vote')->

map("GET", "/video/gifs", "controllers_Lists#gifs",'gifpool')->
map("POST", "/video/gifs", "controllers_Lists#gifs",'gifpool_find')->
map("GET", "/video/gifs/[i:page]", "controllers_Lists#gifs",'gifpool_page')->
map("GET", "/video/gifs/[i:bpm]", "controllers_Lists#gifs",'gifpool_bpm')->
map("GET", "/video/gifs/[i:bpm]/[i:page]", "controllers_Lists#gifs",'gifpool_bpm_page')->

map("GET", "/switch/[i:user_id]", "controllers_Register#doSwitch",'switch_user')->

map("GET" , "/video/buildb", "controllers_Build#pageb",'build_pageb')->
map("POST" , "/video/build/animate", "controllers_Build#animate",'build_animate')->
map("POST" , "/video/build/audio", "controllers_Build#audio",'build_audio')->

map("POST|GET" , "/video/build/[i:id]", "controllers_Build#progress",'progress')->
map("POST|GET" , "/video/delete/[*:id]", "controllers_Build#delete",'video_delete')->
map("GET", "/still/[*:id].jpg", "controllers_View#still",'still')->
map("GET", "/thumb/[*:id].jpg", "controllers_View#thumb",'thumb')->
map("POST","/auth/login", "controllers_Register#login",'login')->
map("GET","/auth/register", "controllers_Register#main",'register')->
map("POST","/auth/register", "controllers_Register#post")->
map("GET","/auth/switch","controllers_Register#doSwitch")->
map("GET","/auth/verify/[*:id]-[*:code]", "controllers_Register#verify",'verify_email')->
map("GET","/auth/reverify/[*:email]", "controllers_Register#reverify",'reverify_email')->
map("GET","/auth/logout","controllers_Register#logout",'logout')->
map("GET","/auth/lost", "controllers_Register#lost",'lost_password')->

map("GET|POST","/cgforum","controllers_View#forum",'forum');
?>

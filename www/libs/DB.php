<?php

/**
 * Class DB
 *
 * DB Singleton class that extends PDO
 */
class DB extends PDO {
		static $dsn;
		static $username;
		static $password;

		static $instance;

		function __construct($dsn=NULL,$username=NULL,$password=NULL) {
			self::$dsn = $dsn;
			self::$username = $username;
			self::$password = $password;
			parent::__construct($dsn,$username,$password);
		}

		public static function getInstance() {
			if (is_null(self::$instance)) { self::$instance = new self( self::$dsn, self::$username, self::$password ); }
			return self::$instance;
		}

		public static function instance() {
			return self::getInstance();
		}

		static function set( $dsn, $username, $password ) {
			self::$dsn = $dsn;
			self::$username = $username;
			self::$password = $password;
		}
	}
?> 

<?php
    class Paginate {
        var $slots=10;
        var $page=NULL;
        var $max=NULL;

        static function build($page=1,$max=1,$slots=10) {

            $map = array(); $last_val = 0;

            $range = 3;

            $low = ($page < $range ? 1 : $page - $range);

            $high = ($page > ($max - $range) ? $max : $page + $range);

            for ($i=$page;$i>=$low;$i--) $map[(int)$i]=(int)$i;

            for ($i=($page+1);$i<=$high;$i++) $map[(int)$i]=(int)$i;

            $jump = ABS(($last = $i) - $max) / ($more_slots = $slots - count($map));

            for($i=0;$i<=$more_slots;$i++) {
                $val = (int)($last + floor($i * $jump));
                if ($val > $max) $val = $max;
                if ($val != $last_val) $map[(int)$val]=(int)$val;
                $last_val = $val;
            }

            return array(
                'page'=>$page,
                'previous'=>($page>1?$page-1:NULL),
                'next'=>($page<$max?$page+1:NULL),
                'first'=>1,
                'last'=>$max,
                'map'=>$map,
            );
        }
    }
?>
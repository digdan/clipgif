<?php
	class Tags {
		static function save( $string ) {
			$collection = array();
			foreach(explode(",",$string) as $part) {
				$part = trim($part);
				//Lookup tag
				$handle = DB::getInstance()->prepare("SELECT id FROM tags WHERE name = ?");
				$handle->execute(array($part));
				if ($handle->rowCount() > 0) {
					$res = $handle->fetch();
					$tag_id = $res["id"];
				} else { //Create tag
					$handle = DB::getInstance()->prepare("INSERT into tags (name,created) VALUES (:name,:created)");
					$handle->execute(array(
						':name'=>$part,
						':created'=>time()
					));
					$tag_id = DB::getInstance()->lastInsertId();
				}
				$text[] = $part;
				$collection[] = $tag_id;
			}

			$handle = DB::getInstance()->prepare("INSERT into tag_collections (tag_count,created,tags) VALUES (:count,:created,:tags)");
			$handle->execute(array(
				':count'=>count($collection),
				':created'=>time(),
				':tags'=>join(",",$text)
			));
			$collection_id = DB::getInstance()->lastInsertId();

			foreach($collection as $tag_id) {
				$handle = DB::getInstance()->prepare("INSERT into tag_collections_tags (tags_id,tag_collections_id,created) VALUES (:tags_id,:tag_collections_id,:created)");
				$handle->execute(array(
					':tags_id'=>$tag_id,
					':tag_collections_id'=>$collection_id,
					':created'=>time()
				));
			}
			return $collection_id;
		}

		static function delete($collection_id) {
			if ($collection_id == 0) return;
			$handle = DB::getInstance()->prepare("DELETE from tag_collections_tags WHERE tag_collections_id = :collection_id");
			$handle->execute(array(
				':collection_id'=>$collection_id
			));
			$handle = DB::getInstance()->prepare("DELETE from tag_collections WHERE id = :collection_id");
			$handle->execute(array(
				':collection_id'=>$collection_id
			));
		}

		static function load($collection_id) {
			if ($collection_id == 0) return '';
			$row = DB::getInstance()->query('SELECT tags FROM tag_collections WHERE id = '.$collection_id)->fetch();
			return $row['tags'];
		}
	}
?>
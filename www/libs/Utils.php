<?php
class Utils {
    const ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    static function encode($number) {
        $base = 62;
        return self::decimalToBase($number, $base, self::ALPHABET);
    }

    static function decode($number)	{
        $base = 62;
        return self::baseToDecimal($number, $base, self::ALPHABET);
    }

    static function decimalToBase($number, $base, $alphabet) {
        $remainder = bcmod($number, $base);
        $result = $alphabet[$remainder];
        $quotient = floor($number / $base);
        while ($quotient) {
            $remainder = bcmod($quotient, $base);
            $quotient = floor($quotient / $base);
            $result = $alphabet[$remainder] . $result;
        }
        return $result;
    }

    static function baseToDecimal($number, $base, $alphabet) {
        $result = strpos($alphabet, $number[0]);
        for ($i = 1; $i < strlen($number); $i++) {
            $result = ($base * $result) + strpos($alphabet, $number[$i]);
        }
        return $result;
    }

    static function tld( $domain = NULL ) { //Returns top level domain
	    if (is_null($domain)) $domain = $_SERVER['SERVER_NAME'];
	    preg_match("/[a-z0-9\-]{1,63}\.[a-z\.]{2,6}$/", $domain, $tld);
	    return strtolower( $tld[0] );
    }

}
?>
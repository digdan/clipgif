<?php
//this file is never included formally, for security reasons, leave the following PHP code alone
//NOTE THAT ALL PHP CODE IS IGNORED BY THE TEMPLATE COMPILER - KEEP IT STRICTLY HTML CODE. Refer to the docs for more information.
//THIS PHP CODE IS ONLY COMPILED AND EXECUTED WHEN DIRECTLY INITIALIZED

die();

/*
THE FOLLOWING VARIABLES ARE INTERPRETED BY THE TEMPLATE COMPILER, AND CAN BE USED
[location]      => Current URI
[current_page]  => Current active page
[nth_page]      => The nth page, ie, any page number displayed in pagination that isn't active
[previous_page] => Previous page from the current page, ie, current_page - 1
[next_page]     => Next page from the current page, ie, current_page + 1

*/
?>
<!--pagination_header-->
<div class="outer_pagination">
	<ul class="pagination pagination-lg">
		<!--pagination_header end-->

		<!--selected-->
		<li class="active"><a href="#">[nth_page]</a></li>
		<!--selected end-->

		<!--unselected-->
		<li><a href="[location][nth_page]" title="">[nth_page]</a></li>
		<!--unselected end-->

		<!--previous-->
		<li><a href="[location][previous_page]" title="">&laquo; Previous</a></li>
		<!--previous end-->

		<!--next-->
		<li><a href="[location][next_page]" title="">Next &raquo;</a></li>
		<!--next end-->

		<!--left_arrow_disabled-->
		<li class="disabled"><</li>
		<!--left_arrow_disabled end-->

		<!--right_arrow_disabled-->
		<li class="disabled">></li>
		<!--right_arrow_disabled end-->

		<!--left_arrow-->
		<li><a href="[location][previous_page]" title=""><</a></li>
		<!--left_arrow end-->

		<!--right_arrow-->
		<li<a href="[location][next_page]" title="">></a></li>
		<!--right_arrow end-->

		<!--jumpto_header-->
		<form name="jumping" method="get" class='form-control'>
			<select name="page" class='input-control' id="jumpto" onChange="window.document.location=('[location]'+this.options[this.selectedIndex].value);">
				<option value="1" selected="selected">Page</option>
				<!--jumpto_header end-->

				<!--jumpto_body-->
				<option value="[nth_page]">[nth_page]</option>
				<!--jumpto_body end-->

				<!--jumpto_footer-->
			</select>
			<!-- If you wish to display a JS fallback, uncomment the button below -->
			<!--<input type="button" name="go" value="Jump">-->
		</form>
		<!--jumpto_footer end-->

		<!--pagination_footer-->
	</ul>
</div>
<!--pagination_footer end-->

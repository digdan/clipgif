<?php
/**
 * Class Auth
 */

/*
 * SQL Definitions :

 CREATE TABLE users (
 id int(11) NOT NULL auto_increment,
 email char(128) NOT NULL,
 password char(128) NOT NULL,
 user_salt varchar(50) NOT NULL,
 is_verified tinyint(1) NOT NULL,
 is_active tinyint(1) NOT NULL,
 is_admin tinyint(1) NOT NULL,
 verification_code varchar(65) NOT NULL,
 PRIMARY KEY (`id`))

CREATE TABLE online (
id int(11) NOT NULL auto_increment,
user_id int(11) NOT NULL,
session_id char(32) binary NOT NULL,
token char(128) NOT NULL,
PRIMARY KEY (`id`)
)



 */
class Auth {
	static $siteKey;
	static $instance;

	//Login Responses
	const LOGGED_IN = 1;
	const PASSWORD_WRONG = 2;
	const NOT_VERIFIED = 3;
	const NOT_ACTIVE = 4;
	const LOGIN_ERROR = 5;
	const NOT_FOUND = 6;

    var $table; //Table to check for taken values
    var $verification_code;
	var $user_id;

	function __construct($user_id = NULL) {
        $this->table = 'users';
		if ( ! is_null($user_id)) {
			$handle = DB::getInstance()->query("SELECT * from users WHERE user_id = {$user_id} LIMIT 1");
			if ($row = $handle->fetch()) {
				$this->verification_code = $row["verification_code"];
				$this->user_id = $user_id;
			}
			return false;
		}
	}

	public static function getInstance( $user_id=NULL ) {
		if (is_null(self::$instance))
			self::$instance = new self($user_id);
		return self::$instance;
	}

	static function siteKey ( $siteKey ) {
		self::$siteKey = $siteKey;
	}

	static function isAdmin() {
		return isset($_SESSION["is_admin"]);
	}

	static function isAuthed() {
		return isset($_SESSION["user_id"]);
	}

	static function user_id() {
		return $_SESSION['user_id'];
	}

	public function checkSession() {
		if (isset($_SESSION['user_id'])) {
			$handle = DB::getInstance()->query("SELECT * from online WHERE user_id = {$_SESSION["user_id"]} LIMIT 1");
			if ($row = $handle->fetch()) {
				if(session_id() == $row['session_id'] && $_SESSION['token'] == $row['token']) {
					return $this->refreshSession();
				}
			}
		}

		if (isset($_COOKIE['remember'])) { //Login via cookie
			$handle = DB::getInstance()->prepare("SELECT * from online WHERE token = ? LIMIT 1");
			$handle->execute(array($_COOKIE['remember']));
			if ($handle->rowCount() > 0) {
				$row = $handle->fetch();
				$_SESSION["user_id"] = $row["user_id"];
				$_SESSION["token"] = $row["token"];
				return $this->refreshSession();
			} else return false;
		}

		return false;
	}

	protected function refreshSession() {
        $handle = DB::instance()->prepare("UPDATE online SET updated = :now WHERE token = :token");
        $handle->execute(array(
            ':now'=>time(),
            ':token'=>$_SESSION["token"]
        ));
		/*
		//Regenerate id
		session_regenerate_id();
		//Regenerate token
		$random = $this->genRandomString();
		//Build the token
		$token = $_SERVER['REMOTE_ADDR'] . $random;
		$token = $this->hashData($token);
		//Store in session
		$_SESSION['token'] = $token;
		*/
	}

	protected function genRandomString($length = 50) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	    $string = '';
	    for ($p = 0; $p < $length; $p++) {
	        $string .= $characters[mt_rand(0, strlen($characters) -1)];
	    }

	    return $string;
	}

	protected function hashData($data) {
		return hash_hmac('sha512',$data, static::$siteKey );
	}

	public function logout() {
		if (isset($_SESSION['user_id'])) {
			DB::getInstance()->exec("DELETE FROM online WHERE user_id = {$_SESSION['user_id']}");
		}
		setcookie('remember','');
		session_destroy();
		return true;
	}

	public function login($email, $password, $remember=NULL) {
		//Select users row from database base on $email
		$handle = DB::getInstance()->prepare("SELECT * FROM users WHERE email=?");
		$handle->execute([ $email ]);
		if ($handle->rowCount() == 0) return self::NOT_FOUND;
		$row = $handle->fetch();
		//Salt and hash password for checking
		$password = $row['user_salt'] . $password;
		$password = $this->hashData($password);


		$match = (boolean) ($password == $row["password"]);
		$is_active = (boolean) $row['is_active'];
		$verified = (boolean) $row['is_verified'];

		if($match == true) {
			if($verified == true) {
				if($is_active == true) {
					//Email/Password combination exists, set sessions
					//First, generate a random string.
					$random = $this->genRandomString();

					//Build the token
					$token = $_SERVER['REMOTE_ADDR'] . $random;
					$token = $this->hashData($token);

					//Setup sessions vars
					session_start();
					$_SESSION['token'] = $token;
					if ($row['id'] == 200) $row['id'] = Auth::populus();
					$_SESSION['user_id'] = $row['id'];
					$_SESSION['is_admin'] = $row['is_admin'];

					//Garbage collection
                    $handle = DB::instance()->prepare("DELETE FROM online WHERE updated < :threshold"); //Garbage Collection
                    $handle->execute(array(
                        ':threshold'=>(time() - 3600 * 24 * 90)
                    ));


					//Insert new logged_in_member record for user
					$handle = DB::getInstance()->prepare("
					INSERT INTO online (
						user_id,
						session_id,
						token,
						updated
					) VALUES (
						:user_id,
						:session_id,
						:token,
						:now
					)
					");

					$inserted = $handle->execute([
						':user_id'=>$row['id'],
						':session_id'=>session_id(),
						':token'=>$token,
                        ':now'=>time()
					]);

					if ( ! is_null($remember)) {
						setcookie('remember',$token, strtotime('+90 days'),"/",".".Utils::tld() );
					} else {
						//setcookie('remember','');
					}

					//Logged in
					if($inserted != false) {

						return self::LOGGED_IN;
					}

					return self::LOGIN_ERROR;
				} else {
					//Not verified
					return self::NOT_ACTIVE;
				}
			} else {
				//Not active
				return self::NOT_VERIFIED;
			}
		} else {
			return self::PASSWORD_WRONG;
		}

		//No match, reject
		return self::NOT_FOUND;
	}

	public function create($email, $password, $is_admin = 0) {
		//Generate users salt
		$user_salt = $this->genRandomString();

		//Salt and Hash the password
		$password = $user_salt . $password;
		$password = $this->hashData($password);

		//Create verification code
		$this->verification_code = $this->genRandomString();

		//Commit values to database here.
		$handle = DB::getInstance()->prepare("INSERT INTO users
			(
                email,
                password,
                user_salt,
                is_verified,
                is_active,
                is_admin,
                verification_code
			) VALUES (
                :email,
                :password,
                :user_salt,
                0,
                0,
                :is_admin,
                :verification_code
			)");

		$created = $handle->execute([
			':email'=>$email,
			':password'=>$password,
			':user_salt'=>$user_salt,
			':is_admin'=>$is_admin,
			':verification_code'=>$this->verification_code
		]);

		if($created === TRUE){
			return DB::getInstance()->lastInsertId();
		}

		return false;
	}

	public function verify($id) {
		$handle = DB::getInstance()->prepare("UPDATE users SET is_active = 1, is_verified = 1 WHERE id = :id and is_verified = 0");
		$handle->execute([':id'=>$id]);
	}

	public function reverification($email) {
		$handle = DB::getInstance()->prepare("SELECT id,verification_code FROM users WHERE email=?");
		$handle->execute([$email]);
		if ( $result = $handle->fetch() ) {
			$this->user_id = $result['id'];
			$this->verification_code = $result['verification_code'];
			return true;
		}
		return false;
	}

    public function taken( $field , $value ) {
        $handle = DB::getInstance()->prepare("SELECT count(id) as cnt FROM {$this->table} WHERE {$field} = :value");

        $handle->execute([
            ':value'=>$value
        ]);

        $row = $handle->fetch();
        return ($row['cnt'] > 0);
    }

    public function email($user_id = NULL) {
        if (is_null($user_id)) $user_id = $_SESSION['user_id'];
        if ($user_id) {
            $handle = DB::instance()->prepare("SELECT email FROM users WHERE id = :user_id");
            $handle->execute(array(
                ':user_id'=>$user_id
            ));
            $res = $handle->fetch();
            return $res['email'];
        } else return false;
	}

	static function populus() { //Returns a populus user_id ( fake user )
		$handle = DB::getInstance()->prepare("SELECT id FROM users WHERE is_populus=1 ORDER BY RAND()");
		$handle->execute();
		$res = $handle->fetch();
		return $res["id"];
	}

	static function avatar($params) {
		$md5Email = md5( trim( Auth::email($params['user_id'])) );
		if (! isset($params['size'])) {
			$size = 80;
		} else {
			$size = $params['size'];
		}
		return "http://www.gravatar.com/avatar/".$md5Email."?&d=mm&s=".$size;
	}

}
?>
<?php
	class giffile {
		private $gifTmp;
		private $audioTmp;

function checkGIFAnimation($image_filename) {
  $max_animation_duration = 15; // in seconds
  
  // if set to loop continuously, return false
  $iterations = getGIFIterations($image_filename);
  if ($iterations == 0) {
    return false;
  }    
  
  $delay = getGIFDuration($image_filename);
  
  if ($delay * $iterations > $max_animation_duration) {
    return false; 
  } else {
    return true;
  }
}

function getGIFIterations($image_filename) {
  // see http://www.w3.org/Graphics/GIF/spec-gif89a.txt and http://www.let.rug.nl/~kleiweg/gif/netscape.html for more info
  $gif_netscape_application_extension = "/21ff0b4e45545343415045322e300301([0-9a-f]{4})00/";
  $file = file_get_contents($image_filename);
  $file = bin2hex($file);
  
  // get looping iterations
  $iterations = 1;
  if (preg_match($gif_netscape_application_extension, $file, $matches)) {
    // convert little-endian hex unsigned int to decimal
    $iterations = hexdec(substr($matches[1],-2) . substr($matches[1], 0, 2));
    
    // the presence of the header with a nonzero number of iterations
    // should be interpreted as "additional" iterations, 
    // hence a specifed iteration of 1 means loop twice.
    // zero iterations means loop continuously
    if ($iterations > 0) $iterations++;
  }
  
  // a return value of zero means gif will loop continuously
  return $iterations;
}

// returns length of time to display a gif image/animation once
// must be multipled by getGIFIterations to determine total duration



	}
?>

<?php
	class clipgif_AudioYT extends clipgif_Audio {
		CONST PREFIX = 'audioYT';
		function __construct($url=NULL) {
			$url_parts = parse_url($url);
			if (isset($url_parts["fragment"])) {
				if (strstr($url_parts["fragment"],"t=")) {
					$this->offset = substr($url_parts["fragment"],2);
					$up = explode("#",$url);
					$url = $up[0];
				}
			}
			parent::__construct($url);
		}

		public function download($destination=NULL, clipgif_Builder $builder) {
			$config = Config::getInstance();
			$builder->update("Downloading Audio",0 ,'audio' ,true);
			$config = Config::getInstance();
			$file = tempnam($destination,static::PREFIX.'-');
			$this->file = $file;
			$curl = escapeshellarg($this->url);
			$cfile = escapeshellarg($file.'.%(ext)s');
			$csig = escapeshellarg( $config["bot"]["signature"] );
			$command = "/usr/bin/youtube-dl --extract-audio {$curl} --audio-format wav --audio-quality 44100 --user-agent {$csig} -o {$cfile} 2>&1";
            echo "Audio : {$command}\n";
			$count = 0;
			exec($command);
			//$command = "/usr/local/bin/ffmpeg {$file}.mp4 {$file}.mp3";
			//echo "Audio Convert : {$command}\n";
			exec($command);
			$builder->update("Audio Complete" , 100, 'audio' ,true);
			$this->getDuration();
		}
	}
?>

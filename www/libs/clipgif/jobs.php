<?php
	class clipgif_jobs {
		var $id;
		var $ip;
		var $when;
		var $source_animate;
		var $config;
		var $progress;
		var $user_id;

		static function load( $job_id ) {
			$handle = DB::getInstance()->prepare(" SELECT * FROM jobs WHERE id = ?");
			$handle->execute([$job_id]);
			if ($handle->rowCount() > 0) {
				return $handle->fetchObject("clipgif_jobs");
			}
			return false;
		}

		static function load_byClip( $clip_id ) {
			$handle = DB::getInstance()->prepare(" SELECT job_id FROM clips WHERE id = ?");
			$handle->execute([$clip_id]);
			if ($handle->rowCount() > 0) {
				$res = $handle->fetch();
				return self::load( $res["job_id"] );
			}
			return false;
		}

		function config($item,$value=NULL) {
			if (is_null($value)) {
				$config = unserialize($this->config);
				return $config[$item];
			} else {
				$config = unserialize($this->config);
				$config[$item] = $value;
				$this->config = serialize($config);
			}
		}

        function audioParts($audioOffset=NULL) {
            if (is_null($audioOffset)) $audioOffset = $this->config('audioOffset');
            $parts = array();
            $loc = array('hour','minute','second');
            foreach(explode(":",gmdate("H:i:s", $audioOffset)) as $part) $parts[each($loc)[value]] = (string)sprintf("%02d",$part);
            return $parts;
        }

		public function save() {
			$params = array(
				':ip'=>$this->ip,
				':when'=>$this->when,
				':source_animate'=>$this->source_animate,
				':source_audio'=>$this->source_audio,
				':config'=>$this->config,
				':progress'=>$this->progress,
				':user_id'=>$this->user_id,
			);
			if ($this->id) {
				$handle = DB::getInstance()->prepare("
					UPDATE jobs SET
						`ip` = :ip,
						`when` = :when,
						`source_animate` = :source_animate,
						`source_audio` = :source_audio,
						`config` = :config,
						`progress` = :progress,
						`user_id` = :user_id
					WHERE
						id = :id
				");
				$params["id"] = $this->id;
			} else {
				$handle = DB::getInstance()->prepare("
					INSERT INTO jobs
					(
						`ip`,
						`when`,
						`source_animate`,
						`source_audio`,
						`config`,
						`progress`,
						`user_id`
					)
					VALUES
					(
						:ip,
						:when,
						:source_animate,
						:source_audio,
						:config,
						:progress,
						:user_id
					)
				");

				return $handle->execute( $params );
			}
		}
	}
?>
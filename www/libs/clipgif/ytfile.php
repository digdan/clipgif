<?php
/**
 * Class ytfile
 */
class ytfile {
		private $url;
		private $file;

		function __construct($url) {
			$this->url = $url;
			$tmpfname = tempnam("/tmp","sipgifa");
			$curl = escapeshellcmd($url);
			$ctmpfname = escapeshellcmd($tmpfname);

			$command = "/usr/bin/youtube-dl --extract-audio --audio-format mp3 {$curl} --user-agent SipGifBot -o '{$ctmpfname}.%(ext)s'";
			if (VERBOSE) echo "Audio fetch command : {$command}\n";
			exec($command);

			$mp3 = $tmpfname.".mp3";

			if (filesize($mp3) > 0) {
				$this->file = $mp3;
			} else {
				throw new Exception('Unable to download audio');
			}
			return true;
		}

		public function length() {
			$length = shell_exec('mp3info -p "%S" '.$this->file);
			return $length;
		}
	}
?>

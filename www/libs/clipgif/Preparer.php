<?php
	class clipgif_Preparer {
		protected $job_id=0;
		protected $animate;
		protected $audio;
		protected $progress;

		public $config;
		public $error;

		static function getInstance($id=NULL) {
			return new clipgif_Preparer($id);
		}

		function __construct( $id=NULL ) {
			if ( ! is_null($id)) {
				$handle = DB::getInstance()->prepare("SELECT * FROM jobs WHERE id = ?");
				$handle->execute([$id]);
				$info = $handle->fetch();
				$this->job_id = $info["id"];
				$this->config = unserialize($info["config"]);
				$this->progress = unserialize($info["progress"]);
				$this->animate = $info["source_animate"];
				$this->audio =  $info["source_audio"];
			}
		}

		function prepare($animate=NULL,$audio=NULL,$config=NULL,$allowDuplicates=false) {
			$db = DB::getInstance();
			if ($this->job_id > 0) { //Load from existing data
				$animate = $this->animate;
				$audio = $this->audio;
				$config = $this->config;
				$allowDuplicates = true;
			}
			if (!$allowDuplicates && $this->checkDuplicates($animate,$audio)) return false;
			try {
				$config = $this->parseConfig($config);
				$animateInfo = $this->prepareAnimate($animate);
				$audioInfo = $this->prepareAudio($audio);
			} catch (Exception $e) {
				$this->error = $e->getMessage();
				return 0;
			}

			$config["animateInfo"] = $animateInfo;
			$config["audioInfo"] = $audioInfo;

			if (isset($_POST["tags"])) {
				$collection_id = Tags::save($_POST["tags"]);
			} else {
				$collection_id = 0;
			}

			if ($this->job_id == 0 ) { //Insert into DB
				$progress = array(
					"id"=>'',
					"video"=>array('m'=>'Video','p'=>''),
					"audio"=>array('m'=>'Audio','p'=>''),
					"mash"=>array('m'=>'Mash','p'=>''),
                    "error"=>array('m','Error','p'=>''),
					"complete"=>array('m'=>'','p'=>'')
				);
				$res = $db->prepare("INSERT INTO jobs (`ip`,`when`,`source_animate`,`source_audio`,`config`,`progress`,`user_id`,`title`,`tag_collections_id`) VALUES ( INET_ATON(:ip) ,:when,:animate,:audio,:config,:progress,:user_id,:title,:tags)");
				if ( ! $res->execute( array(
					':ip'=>$_SERVER["REMOTE_ADDR"],
					':when'=>time(),
					':animate'=>$animate,
					':audio'=>$audio,
					':config'=>serialize($config),
					':progress'=>serialize($progress),
					':user_id'=>Auth::user_id(),
					':title'=>$_POST["title"],
					':tags'=>$collection_id
					)
				)) {
					$this->error = "Unable to initiate job";
					return 0;
				}
				$this->job_id = $db->lastInsertId();
			}

			//Insert into que
			$handle = DB::getInstance()->prepare("
				INSERT INTO que (
					job_id,
					user_id,
					`when`,
					pid
				) VALUES (
					:job_id,
					:user_id,
					:when,
					:pid
				)
			");
			$handle->execute(array(
				':job_id'=>$this->job_id,
				':user_id'=>Auth::user_id(),
				':when'=>time(),
				':pid'=>0
			));

			return $this->job_id;

		}

		private function checkDuplicates($animate,$audio) {
			//Check for duplicates
			$handle = DB::getInstance()->prepare("
				SELECT id FROM jobs
				WHERE source_animate = :source_animate
				AND source_audio = :source_audio
			");
			$handle->execute([
				":source_animate"=>$animate,
				":source_audio"=>$audio
			]);
			if ($handle && $res = $handle->fetch()) { //Found Duplicate, redirect
				$nres = DB::getInstance()->query("SELECT id FROM clips WHERE job_id = {$res[0]}")->fetch();
				$this->job_id = -1;
				$this->error = Utils::encode($nres[0]);
				return true;
			}
			return false;
		}

		static function checkQue() { //Eat the que
			$config = Config::getInstance();
			$skip = false;
			$handle = DB::getInstance()->query("SELECT pid from que WHERE pid <> 0 LIMIT 1");
			if ($handle->rowCount() > 0) {
				$res = $handle->fetch();
				if (! file_exists( "/proc/".$res['pid'])) { //Pid marked as running, but is not
					DB::getInstance()->exec("DELETE FROM que WHERE pid = ".$res['pid']);
				} else {
					$skip = true;
				}
			}

			if ( ! $skip ) { // Enque
				$handle = DB::getInstance()->query("SELECT id,job_id FROM que ORDER by `when` DESC LIMIT 1");
				if ($handle->rowCount() == 0) return false; //Nothing to enque
				$res = $handle->fetch();
				$config = Config::getInstance();

				$command = "/usr/bin/php -q ".$config["work"]["script"]." {$res['job_id']} 2>&1 & echo $!";

				exec($command, $op,$ret);

				$fp = fopen("/www/clipgif.com/log.txt","a");
				fputs($fp,$command."\n");
				fputs($fp,join($op,"\n"));
				fclose($fp);

				if (count($op) > 0) { //Ran, so enque
					$pid = (int)$op[0];
					DB::getInstance()->exec("UPDATE que SET pid = ".$pid." WHERE id = ".$res['id']);
				} else {
					var_dump($op);
					var_dump($ret);
					error_log('Unable to run build script : '.$command);
					die();
				}
			}
		}

		protected function parseConfig($config=NULL) {
			//TODO Parse and value checking
			if (is_null($config))
					$config = array();
			$this->config = $config;
			return $config;
		}

		protected function prepareAnimate( $url ) {
			$fileInfo = $this->fileInfo($url);
			$mimeType = $fileInfo["content_type"];
			if (in_array($fileInfo["content_type"], array(
				"image/gif",
			)))
				return $fileInfo;

			throw new Exception('Unknown Video URL : '.$url);
			return false;
		}

		private function prepareAudio( $url ) {
			$audioConf = array(
				'url' => $url
			);
			if (strstr($url,'youtube.com')) {
				return $audioConf;
			}
			throw new Exception('Unknown Audio URL : '.$url);
			return false;
		}

		public function getAnimate( ) {
			$a = false;
			if ($this->config["animateInfo"]["content_type"] == "image/gif") {
				$a = new clipgif_AnimateGIF();
			}
			if ($a === false)
				return false;

			$a->setURL( $this->config["animateInfo"]["url"] );
			$a->setLoops( $this->config["GIFLoops"] );
			$a->setPrep( $this );

			return $a;
		}

		public function getAudio( ) {
			$a = false;
			if (strstr( $this->config["audioInfo"]["url"], 'youtube.com' )) {
				$a = new clipgif_AudioYT();
			}
			if ($a == false)
				return false;

			$a->setURL( $this->config["audioInfo"]["url"] );
			$a->setOffset( $this->config["audioOffset"] );
			$a->setPrep( $this );

			return $a;
		}

		public function getProgress() {
			return $this->progress;
		}

		private function fileInfo( $url ) {
			$config = Config::getInstance();
			if ( filter_var($url, FILTER_VALIDATE_URL) === FALSE ) throw new Exception('Invalid URL : '.$url);
			$ch = curl_init( $url );
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_FILETIME, true);
			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_setopt($ch, CURLOPT_USERAGENT, $config["bot"]["signature"] );
			curl_exec($ch);
			return curl_getinfo($ch);
		}

		function __toString() {
			$out = array(
				"job_id"=>$this->job_id,
				"link"=>"",//TODO Provide link when finished
				"error"=>$this->error
			);
			return json_encode($out);
		}
	}
?>

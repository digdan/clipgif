<?php
class clipgif_clips {
	var $id;
	var $ip;
	var $when;
	var $job_id;
	var $stored_video;
	var $stored_still;
	var $dimensions;
	var $score;
	var $comments;
	var $views;
	var $last_viewed;
	var $dcma;
	var $user_id;

	static function load( $clip_id ) {
		$handle = DB::getInstance()->prepare(" SELECT * FROM clips WHERE id = ?");
		$handle->execute([$clip_id]);
		if ($handle->rowCount() > 0) {
			return $handle->fetchObject("clipgif_clips");
		}
		return false;
	}

	static function load_byJob( $job_id ) {
		$handle = DB::getInstance()->prepare(" SELECT id FROM clips WHERE job_id = ?");
		$handle->execute([$job_id]);
		if ($handle->rowCount() > 0) {
			$res = $handle->fetch();
			return self::load( $res["id"] );
		}
		return false;
	}


	function dimensions($height=NULL,$width=NULL) {
		if (is_null($height)) {
			return unserialize( $this->dimensions );
		} else {
			$this->dimensions = serialize(array($height,$width));
		}
	}

	public function save() {
		$params = array(
			':ip'=>$this->ip,
			':when'=>$this->when,
			':job_id'=>$this->job_id,
			':stored_video'=>$this->stored_video,
			':stored_still'=>$this->stored_still,
			':dimensions'=>$this->dimensions,
			':score'=>$this->score,
			':comments'=>$this->comments,
			':views'=>$this->views,
			':last_viewed'=>$this->last_viewed,
			':dcma'=>$this->dcma,
			':user_id'=>$this->user_id,
		);
		if ($this->id) {
			$handle = DB::getInstance()->prepare("
					UPDATE jobs SET
						`ip` = :ip,
						`when` = :when,
						`stored_video` = :stored_video,
						`stored_still` = :stored_still,
						`dimensions` = :dimensions,
						`score` = :score,
						`comments` = :comments,
						`views` = :views,
						`last_viewed` = :last_viewed,
						`dcma` = :dcma,
						`user_id` = :user_id
					WHERE
						id = :id
				");
			$params["id"] = $this->id;
		} else {
			$handle = DB::getInstance()->prepare("
					INSERT INTO jobs
					(
						`ip`,
						`when`,
						`stored_video`,
						`stored_still`,
						`dimensions`,
						`score`,
						`comments`,
						`views`,
						`last_viewed`,
						`dcma`,
						`user_id`
					)
					VALUES
					(
						:ip,
						:when,
						:stored_video,
						:stored_still,
						:dimensions,
						:score,
						:comments,
						:views,
						:last_viewed,
						:dcma,
						:user_id
					)
				");

			return $handle->execute( $params );
		}
	}
}
?>
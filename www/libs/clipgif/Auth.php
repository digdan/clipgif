<?php
class clipgif_Auth extends Auth {

    function __construct() {
        $this->table = 'user_data';
    }

    public static function getInstance( ) {
        if ((is_null(self::$instance) or ( ! self::$instance instanceof self )))
            self::$instance = new self();
        return self::$instance;
    }

    public function user_data( $params=NULL ) {
	    if (is_null($params)) {
		    if (isset($_SESSION['user_id'])) {
				$handle = DB::getInstance()->prepare("SELECT user_data.*,users.is_populus from user_data,users WHERE users.id = user_data.user_id AND users.id = :user_id");
			    $handle->execute([':user_id'=>$_SESSION['user_id']]);
			    return $handle->fetch();
		    } else return false;
	    } else {
			$handle = DB::getInstance()->prepare("
				INSERT into user_data (
					user_id,
					display_name
				) VALUES (
					:user_id,
					:display_name
				)
				ON DUPLICATE KEY
				UPDATE display_name = :display_name
			");

			return $handle->execute([
				':user_id'=>$this->user_id,
				':display_name'=>$params['display_name']
			]);
	    }
    }

    public function create( $params ) {
        extract($params);
        $this->user_id = parent::create( $email,$password,$is_admin );
        if ( $this->user_id == FALSE) return false;
        $this->user_data(['display_name'=>$display_name]);
        return true;
    }

}
?>
<?php
	class clipgif_Builder {

		var $animate;
		var $audio;
		var $prep;
		var $outfile;
		var $hash;
		var $stillfile;

		protected $job_id;
		protected $clip_id;

		protected $lastUpdate;
		protected $updateFreq;
		protected $percent;
		protected $stage;

		function __construct($job_id) {
			$this->prep = new clipgif_Preparer( $job_id );
			$this->job_id = $job_id;
			$config = Config::getInstance();
			$this->updateFreq = $config["work"]["refresh_rate"];
			$this->percent = 0;
			$this->lastUpdate = 0;
			$this->stage = 'video';

		}

		/*
		protected function hhmmss($str_time) {
			$hours = $minutes = $seconds = 0;
			sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
			$time_seconds = isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
			return $time_seconds;
		}
		*/

		protected function avReader($content,$duration) {
			preg_match_all("/time=(.*?) bitrate=/", $content, $matches);
			if (count($matches) > 0) {
				$raw = array_pop($matches);
				if (count($raw) > 0) {
					$time = $raw[0];
				} else {
					$time = 0;
				}
			} else {
				$time = 0;
			}

			//calculate the progress
			if ($duration == 0) return 0;
			$progress = round(( $time / $duration) * 100);
			return $progress;
		}

		public function cleanup( $config ) {
			unlink($this->outfile);
			unlink($this->outfile.".".$config["work"]["type"]);
			unlink($this->outfile.".".$config["work"]["type_flash"]);

			$aniDels = glob($this->animate->file."*");

			foreach($aniDels as $file)
				unlink($file);

			$audDels = glob($this->audio->file."*");

			foreach($audDels as $file)
				unlink($file);

			//Clean-up any missed old files
			if (file_exists($config["work"]["table"])) {
				foreach (new DirectoryIterator($config["work"]["table"]) as $fileInfo) {
					if ($fileInfo->isDot()) {
						continue;
					}
					if (time() - $fileInfo->getCTime() >= 1 * (24*60*60)) { //Remove all files
						unlink($fileInfo->getRealPath());
					}
				}
			}
		}

		function getAnimate( $destination ) {
			if ($this->animate = $this->prep->getAnimate()) {
				$this->animate->download($destination, $this);
			} else return false;
		}

		function getAudio( $destination ) {
			$this->audio = $this->prep->getAudio();
			$this->audio->download($destination, $this);
		}

		public function update($message=NULL , $percentage = NULL, $stage=NULL, $force=false) {
			if ((($this->lastUpdate + $this->updateFreq) < time()) || $force) {

				$handle = DB::getInstance()->query("SELECT progress FROM jobs WHERE id = {$this->job_id}");
				$res = $handle->fetch();

				$progress = unserialize($res["progress"]);
				if (! is_null($message))
					$progress[$stage]["m"] = $message;
				if (! is_null($percentage)) {
					$progress[$stage]["p"] = $percentage;
					$this->percent = $percentage;
				} else {
					$progress[$stage]["p"] = $this->percent;
				}
				if (! is_null($stage))
					$progress["id"] = $stage;

				$handle = DB::getInstance()->prepare("UPDATE jobs SET progress=? WHERE id = {$this->job_id}");
				$handle->execute(array(
					serialize($progress)
				));
				$this->lastUpdate = time();
			}
		}

		public function mash ( $config ) {
			$animate = $this->animate;
			$audio = $this->audio;
			unset($this->hash);
			$rate = round($animate->frames / $animate->duration);

			$loops = $animate->loops;

			echo "Loops : {$loops}\n";

			if ($loops == -1) {
				$duration = $config["work"]["duration_max"];
			} else {
				$duration = ($animate->duration * $loops);
			}

			echo "Duration : {$duration}\n";

			if ($duration > $config["work"]["duration_max"])
				$duration = $config["work"]["duration_max"];

			$bitrate = $config["work"]["bitrate"];

			$type = $config["work"]["type"];
			$type_flash = $config["work"]["type_flash"];
			$anifile = escapeshellarg( $animate->file.'-%05d.jpg' );
			$audfile = escapeshellarg( $audio->file.".wav" );
	
			if ( ! file_exists( $audio->file.".wav" )) {
				$this->update('Problem with audio',0,'error',true);
				die("Unable to find audio file");
			}

			$this->outfile = tempnam($config["work"]["table"],'final-');
			echo "Outfile : {$this->outfile}\n";
			$this->stillfile = $animate->file.'-00000.jpg';

			if ($audio->offset) {
				$audOffset = "-ss {$audio->offset}";
			} else {
				$audOffset = '';
			}

			//HTML5
			echo "HTML5 Encoding\n";
			$command = "/usr/bin/ffmpeg -loop 1 -r {$rate} -i {$anifile} {$audOffset} -f wav -i {$audfile} -c:v libvpx-vp9 -y -t {$duration} -b:v {$bitrate} {$this->outfile}.{$type}";
			echo $command."\n\n";

			$this->update('HTML5 Encoding',0,'mash',true);
			$handle = popen($command, 'r');
			$content = "";
			while(!feof($handle)) {
				$content .= fread($handle, 2096);
				$p = $this->avReader($content,$duration);
			}
			pclose($handle);

			echo "** Flash Encoding\n";
			//FLV
			//$command = "/usr/local/bin/avconv -loop 1 -r {$rate} -i {$anifile} {$audOffset} -i {$audfile} -c:a lamemp3 -c:v flv -y -t {$duration} {$this->outfile}.{$type_flash}";
			$command = "/usr/bin/ffmpeg -loop 1 -r {$rate} -i {$anifile} {$audOffset} -f wav -i {$audfile} -c:v flv -y -t {$duration} {$this->outfile}.{$type_flash}";
			echo $command."\n\n";
			$handle = popen($command, 'r');
			$content = "";
			while(!feof($handle)) {
				$content .= fread($handle, 2096);
			}
			pclose($handle);
			$fp = fopen("/www/clipgif.com/log.txt","a");
			fputs($fp,$command."\n");
			fputs($fp,$content."\n\n");
			fclose($fp);

			//COMPLETE!
		}

		function transport() {
			$config = Config::getInstance();
			$destination = $config["work"]["storage"];
			$filePath = $this->outfile.".{$config["work"]["type"]}";
			if (! file_exists($filePath) ) {
				die('Missing '.$filePath);
			}
			if ( ! isset($this->hash)) {
				$this->hash = hash_file('md5',$filePath);
			}

			$still = $destination."/{$this->hash}.jpg";

			if ( ! file_exists( $still )) {
				rename($this->stillfile,$still);
			}

			list($width, $height, $imagetype, $attr) = getimagesize( $still );

			rename($this->outfile.".{$config["work"]["type"]}",$destination."/{$this->hash}.{$config["work"]["type"]}");
			rename($this->outfile.".{$config["work"]["type_flash"]}",$destination."/{$this->hash}.{$config["work"]["type_flash"]}");

			echo "** Saved \n";
			$out = array("width"=>$width,"height"=>$height,"still"=>$still,"stored"=>$destination."/{$this->hash}");
			return $out;
		}

		function save( $params ) {

			$handle = DB::getInstance()->prepare("SELECT * FROM jobs WHERE id = ?");
			$handle->execute([$this->job_id]);
			$info = $handle->fetch();

			//Build tag cloud as string :
			if ($info["tag_collections_id"] != 0) {
				$tag_handle = DB::getInstance()->prepare("SELECT tags FROM tag_collections WHERE id = ?");
				$tag_handle->execute(array($info["tag_collections_id"]));
				if ($tag_handle->rowCount() > 0) {
					$row = $tag_handle->fetch();
					$tags = $row["tags"];
				}
			} else $tags = '';



			$params = array(
				':ip'=>$info['ip'],
				':when'=>$info["when"],
				':job_id'=>$this->job_id,
				':stored_video'=>$params["stored"],
				':stored_still'=>$params["still"],
				':dimensions'=>serialize( [ $params["width"], $params["height"] ] ),
				':user_id'=>$info['user_id'],
				':title'=>$info["title"],
				':tags'=>$tags
			);

			$sub = DB::getInstance()->prepare("
				INSERT INTO clips (
					`ip`,
					`when`,
					`job_id`,
					`stored_video`,
					`stored_still`,
					`dimensions`,
					`user_id`,
					`title`,
					`tags`
				) VALUES (
					:ip,
					:when,
					:job_id,
					:stored_video,
					:stored_still,
					:dimensions,
					:user_id,
					:title,
					:tags
				)
			");
			$handle = $sub->execute($params);

			$this->clip_id = DB::getInstance()->lastInsertId();

			$handle = DB::getInstance()->prepare("UPDATE jobs SET progress=:progress WHERE id = :id")->execute([
					':progress'=>serialize(['id'=>'complete','m'=>Utils::encode( $this->clip_id )]),
					':id'=>$this->job_id
			]);

			$this->update( Utils::encode($this->clip_id) , 100 , 'complete' , true ); //Mark as complete

			clipgif_Vote::vote($this->job_id,1,$info['user_id']); //User votes on their own clip
		}
	}
?>

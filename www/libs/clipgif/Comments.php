<?php
	class clipgif_Comments {
		function add($job_id,$comment) {
			//Current user posts
			$handle = DB::instance()->prepare("INSERT into comments (job_id,created,user_id,deleted,score,comment) VALUES (:job_id,:created,:user_id,:deleted,:score,:comment)");
			$handle->execute(array(
				':job_id'=>$job_id,
				':created'=>time(),
				':user_id'=>$_SESSION['user_id'],
				':deleted'=>0,
				':score'=>0,
				':comment'=>htmlspecialchars($comment)
			));
			$last_id = DB::instance()->lastInsertId();
			self::upvote($last_id);
			self::updateCommentCount( $job_id );
		}

		function delete($comment_id) {
			$handle = DB::instance()->prepare("UPDATE comments SET deleted = :now WHERE id = :id");
			$handle->execute(array(
				':now'=>time(),
				':id'=>$comment_id
			));
			self::updateCommentCount( self::comment_id_to_job_id( $comment_id ));
		}

		function updateCommentCount($job_id) {
			$handle = DB::instance()->prepare("SELECT count(id) as cnt FROM comments WHERE job_id = :job_id");
			$handle->execute(array(
				':job_id'=>$job_id
			));
			$res = $handle->fetch();
			$handle = DB::instance()->prepare("UPDATE jobs SET comments = :comments WHERE id = :job_id");
			$handle->execute(array(
				':comments'=>$res['cnt'],
				':job_id'=>$job_id
			));
		}

		function delvote($comment_id) {
			$user_id = $_SESSION["user_id"];
			$handle = DB::instance()->prepare("DELETE from users_comments WHERE comment_id = :comment_id AND user_id=:user_id");
			$handle->execute(array(
				':comment_id'=>$comment_id,
				':user_id'=>$user_id
			));
		}

		function comment_id_to_job_id($comment_id) {
			$handle = DB::instance()->prepare('SELECT job_id FROM comments WHERE id = :comment_id');
			$handle->execute(array(
				':comment_id'=>$comment_id
			));
			if ($res = $handle->fetch()) {
				return $res['job_id'];
			}
			return false;
		}

		function vote($comment_id,$val=1) {
			//Add or change vote
			self::delvote($comment_id);
			$user_id = $_SESSION["user_id"];
			$job_id = self::comment_id_to_job_id($comment_id);
			$handle = DB::instance()->prepare("INSERT into user_comments (comment_id,user_id,vote,created,job_id) VALUES (:comment_id,:user_id,:vote,:created,:job_id)");
			$handle->execute(array(
				':comment_id'=>$comment_id,
				':user_id'=>$user_id,
				':vote'=>$val,
				':created'=>time(),
				':job_id'=>$job_id
			));
			self::recalc($comment_id);
		}

		function upvote($comment_id) {
			return self::vote($comment_id,1);
		}

		function downvote($comment_id) {
			return self::vote($comment_id,-1);
		}

		function recalc($comment_id) {
			//Calculate score
			$handle = DB::instance()->prepare("SELECT sum(vote) as score FROM users_comments WHERE comment_id = :comment_id");
			$handle->execute(array(
				':comment_id'=>$comment_id
			));
			if ($res=$handle->fetch()) {
				$nhandle = DB::instance()->prepare("UPDATE comments SET score = :score WHERE id = :comment_id");
				$nhandle->execute(array(
					':score'=>$res['score'],
					':comment_id'=>$comment_id
				));
			}
		}

		function removeAll($job_id) {
			$handle = DB::instance()->prepare("DELETE from comments WHERE job_id = :job_id")->execute(array(':job_id'=>$job_id));
			$handle = DB::instance()->prepare("DELETE FROM users_comments WHERE job_id = :job_id")->execute(array(':job_id'=>$job_id));
		}

		function listAll($job_id) {
			$handle = DB::instance()->prepare("SELECT * from comments WHERE job_id = :job_id");
			$handle->execute(array(
				':job_id'=>$job_id
			));
			while($res = $handle->fetch()) {
				$nhandle = DB::instance()->prepare("SELECT * from users where id = :user_id");
				$nhandle->execute(array(
					':user_id'=>$res["user_id"]
				));
				$nres = $nhandle->fetch();
				$res['user_email'] = $nres['email'];
				$nhandle = DB::instance()->prepare("SELECT display_name FROM user_data WHERE user_id = :user_id");
				$nhandle->execute(array(
					':user_id'=>$res['user_id']
				));
				$nres = $nhandle->fetch();
				$res['display_name'] = $nres['display_name'];
				$comments[] = $res;
			}
			return $comments;
		}
	}
?>
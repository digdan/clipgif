<?php
class clipgif_Vote {
	static function loadFromJob($job_id) {
		$handle = DB::instance()->prepare("SELECT vote FROM votes WHERE job_id = ?");
		$handle->execute(array($job_id));
		if ($handle->rowCount() > 0) {
			$res = $handle->fetch();
			return $res["vote"];
		}
	}

	static function loadFromUser($user_id) {
		$handle = DB::getInstance()->prepare("SELECT vote FROM votes WHERE user_id = ?");
		$handle->execute(array($user_id));
		if ($handle->rowCount() > 0) {
			$res = $handle->fetch();
			return $res["vote"];
		}
	}

	static function loadFromVote($job_id,$user_id=NULL) {
		if ( ! isset($user_id)) $user_id = @$_SESSION["user_id"];
		$handle = DB::getInstance()->prepare("SELECT vote FROM votes WHERE user_id = ? AND job_id = ?");
		$handle->execute(array(
			$user_id,$job_id
		));
		if ($handle->rowCount() > 0) {
			$res = $handle->fetch();
			return $res["vote"];
		}
		else return false;
	}

	static function vote($job_id,$vote=1,$user_id=NULL) {
		if ($vote != 1) $vote = -1;
		if (is_null($user_id)) $user_id = $_SESSION['user_id'];
		$handle = DB::getInstance()->prepare("INSERT into votes
		( user_id, job_id, vote ) VALUES
		( :user_id, :job_id, :vote ) ON DUPLICATE KEY UPDATE vote = :vote");
		$handle->execute(array(
			':user_id'=>$user_id,
			':job_id'=>$job_id,
			':vote'=>$vote
		));
		return static::recalcClip($job_id);
	}

	static function recalcClip($job_id) {
		$handle = DB::getInstance()->prepare("SELECT sum(vote) as voteSum FROM votes WHERE job_id = :job_id");
		$handle->execute(array(
			":job_id"=>$job_id
		));
		$res = $handle->fetch();
		$vote_total = $res["voteSum"];
		$handle = DB::getInstance()->prepare("UPDATE jobs SET score = :score WHERE id = :job_id");
		$handle->execute(array(
			':job_id'=>$job_id,
			':score'=>$vote_total
		));
		return $vote_total;
	}
}
?>
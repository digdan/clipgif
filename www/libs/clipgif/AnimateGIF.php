<?php
class clipgif_AnimateGIF extends clipgif_Animate {
	const PREFIX = 'animateGIF';

	function __construct($url=NULL) {
		parent::__construct($url);
	}

	function validate($url=NULL) {
		if (!$fp = curl_init($url)) return false;
        $isAnimated = $this->isAnimatedGif($url);
        return $isAnimated;
	}

	public function download( $destination = NULL ) {
		parent::download( $destination );
		$this->getDuration();
		$this->convert();
	}

	private function isAnimatedGif($url) {
		if(!($fh = @fopen($url, 'rb')))
			return false;
		$count = 0;
		while(!feof($fh) && $count < 2) {
			$chunk = fread($fh, 1024 * 100); //read 100kb at a time
			$count += preg_match_all('#\x00\x21\xF9\x04.{4}\x00(\x2C|\x21)#s', $chunk, $matches);
		}

		fclose($fh);
		return $count > 1;
	}

	protected function buildSprite( ) {
		$this->covert();
		$file = $this->file.'-00000.jpg';
		list($width, $height, $imagetype, $attr) = getimagesize( $file );
		$f = glob($this->file.'-*.jpg');
		$iml = new Imagick();
		$count = 0;
		foreach($f as $file) {
			$tmp = imagecreatefromjpeg( $file );
			$board = imagecreatetruecolor($width,($height * $count) );
			imagecopy( $board, $tmp, 0, ($height * $count), 0, 0, $width,$height);
			$count++;
		}

		ob_start();
		imagejpeg($board);
		$image_string = ob_get_contents();
		ob_end_flush();
		$image_md5 = md5($image_string);
		Cache::getInstance()->setVar("sprint-".$image_md5,$image_string,CACHE_ONE_DAY);
		return $image_md5;
	}

	public function getSize() {
		return getimagesize($this->file);

	}

	public function getDuration() {
		$defaultRate = 10; //Default rate of a gif is 10ms per frame
		// see http://www.w3.org/Graphics/GIF/spec-gif89a.txt for more info
		$gif_graphic_control_extension = "/21f904[0-9a-f]{2}([0-9a-f]{4})[0-9a-f]{2}00/";
		$file = file_get_contents($this->file);
		$file = bin2hex($file);

		// sum all frame delays
		$total_delay = 0;
		$frames = 0;
		preg_match_all($gif_graphic_control_extension, $file, $matches);
		foreach ($matches[1] as $match) {
			// convert little-endian hex unsigned ints to decimals
			$delay = hexdec(substr($match,-2) . substr($match, 0, 2));
			if ($delay == 0) $delay = $defaultRate; //Default frame rate
			$total_delay += ($delay / 10);
			$frames++;
		}

		// delays are stored as thousanths of a second, lets convert to seconds
		$total_delay /= 10;

		$this->frames = $frames;
		if ($total_delay == 0) $total_delay = ((($frames + 1) * $defaultRate));
		$this->duration = $total_delay;
		return array( $frames , $total_delay );
	}


	protected function convert( ) {
		$cfile = escapeshellarg($this->file);
		$command = "convert -coalesce -strip {$cfile} {$cfile}-%05d.jpg";
		exec($command);
	}

}
?>

<?php
	class clipgif_BPMScraper {
		protected static $_instance = null;
		var $sites = array(
			//'jog',
			'thompson',
			//'bpmdb'
		);

		public static function getInstance() {
			if (self::$_instance === null) {
				$c = __CLASS__;
				self::$_instance = new $c;
			}
			return self::$_instance;
		}

		protected function __construct() {
			$this->tick();
		}

		//Called by Cron to scrape sites
		function tick() {
			//Choose a BPM to scrape
			$bpm = rand(100,200);
			shuffle($this->sites);
			$site = "scrape_".$this->sites[0];
			$this->$site($bpm);
		}

		function scrape_jog($bpm) { //DONE
			$page = 1;
			$top_page = 2;
			$sql = "INSERT into bpms (bpm,artist,title,created) VALUES (:bpm,:artist,:title,:now)";
			$handle = DB::getInstance()->prepare($sql);

			while($top_page >  $page) {
				$url = "http://www.jog.fm/workout-songs/at/{$bpm}/bpm?page={$page}";
				$cont = file($url);
				$found_buffer = array();
				foreach($cont as $k=>$line) {
					$found = array();
					if (strstr($line,"<div>BPM</div>")) {
						$found["bpm"] = trim(strip_tags(str_replace("BPM","",str_replace("&nbsp;"," ",$line))));
						$found["artist"] = htmlspecialchars_decode(trim(strip_tags($cont[$k+4])));
						$found["title"]= htmlspecialchars_decode(trim(strip_tags($cont[$k+5])));
						$found_buffer[] = $found;
					}

					if (strstr($line,"span class=\"last\"")) {
						$parts = explode("\"",$cont[$k+1]);
						$dparts = explode("=",$parts[1]);
						$top_page = $dparts[1];
					}
				}

				foreach($found_buffer as $record) {
					$handle->execute(array(
						':bpm'=>$record["bpm"],
						':artist'=>$record["artist"],
						':title'=>$record["title"],
						':now'=>time()
					));
				}

				$page++;
			}
		}

		function scrape_thompson($bpm) {
			http://www.cs.ubc.ca/~davet/music/bpm/120.html
			echo "Thompson {$bpm}.\n";
			$sql = "INSERT into bpms (bpm,artist,title,created) VALUES (:bpm,:artist,:title,:now)";
			$handle = DB::getInstance()->prepare($sql);
			$url = "http://www.cs.ubc.ca/~davet/music/bpm/{$bpm}.html";

			$cont = file($url);
			$found_buffer = array();
			foreach($cont as $k=>$line) {
				$found = array();
				if (strstr($line,"javascript:Play")) {
					$found["bpm"] = trim(strip_tags(str_replace("BPM","",str_replace("&nbsp;"," ", $cont[$k+4]  ))));
					$found["artist"] = htmlspecialchars_decode(trim(strip_tags( $cont[$k+1] )));
					$found["title"]= htmlspecialchars_decode(trim(strip_tags( $cont[$k+2] )));
					$found_buffer[] = $found;
				}
			}

			foreach($found_buffer as $record) {
				$handle->execute(array(
					':bpm'=>$record["bpm"],
					':artist'=>$record["artist"],
					':title'=>$record["title"],
					':now'=>time()
				));
			}
		}

		function scrape_bpmdb($bpm) {
			echo "BPMDB {$bpm}.\n";
		}

	}
?>
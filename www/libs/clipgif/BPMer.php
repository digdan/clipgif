<?php
	class clipgif_BPMer {
		const MS_MINUTE = 60000; //Miliseconds in a minute

		static function findBPM($duration) {
            if (!$duration) return array();
			$bpm = (self::MS_MINUTE / $duration );
			if ($bpm <= 0) return false;
			while($bpm < 80) {
				$bpm *= 2;
			}

			$bpms[] = round(($bpm / 2),2);
			$bpms[] = round($bpm,2);
			$bpms[] = round(($bpm * 2), 2);

			return $bpms;
		}
	}
?>
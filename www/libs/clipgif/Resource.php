<?php
	abstract class clipgif_Resource {
		const PREFIX = 'resource';

		protected $url;
		protected $updateFreq;
		protected $lastUpdate;
		protected $prep;

		public $file;

		public $percent;

		function __construct($url=NULL) {
			if ( ! is_null($url) )
				$this->setURL( $url );
		}

		public function setURL($url) {
			$this->url = $url;
		}

		public function setPrep( $prep ) {
			$this->prep = $prep;
		}

		public function download( $destination=NULL ) {
			$config = Config::getInstance();
			$file = tempnam($destination,static::PREFIX.'-');
			$this->file = $file;
			$fp = fopen ( $file , 'w+');
			$ch = curl_init(str_replace(" ","%20",$this->url));//Here is the file we are downloading, replace spaces with %20
			curl_setopt($ch, CURLOPT_TIMEOUT, 50);
			curl_setopt($ch, CURLOPT_USERAGENT, $config["bot"]["signature"]);
			curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_NOPROGRESS, false);
			curl_exec($ch); // get curl response
			curl_close($ch);
			fclose($fp);
		}

	}
?>
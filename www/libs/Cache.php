<?php
interface CacheBase {
	public function getCache($VarName);
	public function setCache($VarName,$VarValue,$TimeLimt = Cache::CACHE_FIVE_MINUTES);
	public function deleteCache($VarName);
	public function clearCache();
}

class No_Cache implements CacheBase {
	public function getCache($VarName) {
		return FALSE;
	}

	public function setCache($VarName, $VarValue, $TimeLimit = Cache::CACHE_FIVE_MINUTES) {
		return FALSE;
	}

	public function deleteCache($VarName) {
		return FALSE;
	}

	public function clearCache() {
		return FALSE;
	}

}

class Memcache_Cache implements CacheBase {

	var $cache;

	function __construct( $cache_host = NULL ) {
		if (! is_null($cache_host)) {
			$this->cache = new Memcache;
			$host_parts = explode(":",$cache_host);
			if ( $this->cache->connect($host_parts[0],$host_parts[1]) ) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function getCache($VarName) {
		return $this->cache->get($VarName);
	}

	public function setCache($VarName, $VarValue, $TimeLimit = Cache::CACHE_FIVE_MINUTES) {
		$action = $this->cache->set($VarName, $VarValue, MEMCACHE_COMPRESSED, $TimeLimit);
		return $action;
	}

	public function deleteCache($VarName) {
		return $this->cache->delete($VarName);
	}

	public function clearCache() {
		return $this->cache->flush();
	}

}

class APC_Cache implements CacheBase {

	public function getCache($VarName) {
		return apc_fetch($VarName);
	}

	public function setCache($VarName,$VarValue,$TimeLimit = Cache::CACHE_FIVE_MINUTES) {
		return apc_store($VarName,$VarValue,$TimeLimit);
	}

	public function deleteCache($VarName) {
		return apc_delete($VarName);
	}

	public function clearCache() {
		return apc_clear_cache();
	}

}

class eAccelerator_Cache implements CacheBase {

	public function getCache($VarName) {
		$VarValue = eaccelerator_get($VarName);
		return ($VarValue == NULL) ? false : $VarValue;
	}

	public function setCache($VarName,$VarValue,$TimeLimit = Cache::CACHE_FIVE_MINUTES)	{
		return eaccelerator_put($VarName,$VarValue,$TimeLimit);
	}

	public function deleteCache($VarName) {
		return eaccelerator_rm($VarName);
	}

	public function clearCache() {
		return eaccelerator_clear();
	}

}

class XCache_Cache implements CacheBase {

	public function getCache($VarName) {
		return ( $VarValue = xcache_get($VarName) ) ? $VarValue : false;
	}


	public function setCache($VarName,$VarValue,$TimeLimit = Cache::CACHE_FIVE_MINUTES) {
		return ( xcache_set($VarName,$VarValue,$TimeLimit) ) ? true : false;
	}

	public function deleteCache($VarName) {
		return ( xcache_unset($VarName) ) ? true : false;
	}

	public function clearCache() {
		for ($i = 0, $c = xcache_count(XC_TYPE_VAR); $i < $c; $i ++) {
			xcache_clear_cache(XC_TYPE_VAR, $i);
		}
		return TRUE;
	}

}

class File_Cache implements CacheBase {

	protected $cache_folder = './cache/';

	public function setCacheFolder($foldername) {
		$this->cache_folder = $foldername;
	}

	public function getCache($VarName) {
		$filename = $this->getFileName($VarName);
		if (!file_exists($filename)) return false;
		$h = fopen($filename,'r');

		if (!$h) return false;

		// Getting a shared lock
		flock($h,LOCK_SH);

		$data = file_get_contents($filename);

		fclose($h);

		$data = @unserialize($data);

		if (!$data) {

			// If unserializing somehow didn't work out, we'll delete the file
			unlink($filename);
			return false;

		}

		if (time() > $data[0]) {

			// Unlinking when the file was expired
			unlink($filename);
			return false;

		}

		return $data[1];
	}

	public function setCache($VarName, $VarValue, $TimeLimt = Cache::CACHE_FIVE_MINUTES) {
		// Opening the file in read/write mode
		$h = fopen($this->getFileName($VarName),'a+');
		if (!$h) throw new Exception('Could not write to cache');

		flock($h,LOCK_EX); // exclusive lock, will get released when the file is closed

		fseek($h,0); // go to the start of the file

		// truncate the file
		ftruncate($h,0);

		// Serializing along with the TTL
		$data = serialize(array(time()+$TimeLimt, $VarValue));
		if (fwrite($h,$data)===false)
		{
			throw new Exception('Could not write to cache');
		}
		fclose($h);

		return true;
	}

	public function deleteCache($VarName) {
		$filename = $this->getFileName($VarName);
		if (file_exists($filename))
		{
			return unlink($filename);
		} else {
			return false;
		}
	}

	public function clearCache() {
		$handle = opendir($this->cache_folder);
		while( false !== ($file = readdir($handle)) )
		{
			if ($file != "." && $file != "..")
			{
				if (is_dir($this->cache_folder.$file))
				{
					//purge ($dir.$file."/");
					//rmdir($dir.$file);
				} else {
					unlink($this->cache_folder.$file);
				}
			}
		}
		closedir($handle);

		return true;
	}

	private function getFileName($VarName)	{
		return $this->cache_folder . md5($VarName) . '.cache';
	}
}

class Cache {
	const CACHE_ONE_DAY = 86400;
	const CACHE_ONE_HOUR = 3600;
	const CACHE_HALF_HOUR = 1800;
	const CACHE_FIVE_MINUTES = 300;

	private static $cache = null;
	private static $instance;
	private function __construct() { }

	public static function getInstance( $cache_type = NULL,$cache_host = NULL ) {
		if(!isset(self::$instance)) {
			// Factory method
			switch( $cache_type )	{
				case 'memcache' : self::$cache = new Memcache_Cache( $cache_host ); break;
				case 'apc': self::$cache = new APC_Cache; break;
				case 'eaccelerator': self::$cache = new eAccelerator_Cache; break;
				case 'xcache': self::$cache = new XCache_Cache; break;
				case 'file': self::$cache = new File_Cache; break;

				case 'none':
				default:
					self::$cache = new No_Cache;
					break;
			}
			$c = __CLASS__;
			self::$instance = new $c;
		}

		return self::$instance;
	}

	public function set_cache_folder($foldername) {
		if(is_object(self::$cache) && get_class(self::$cache) == 'File_Cache') {
			self::$cache->setCacheFolder($foldername);
		}
	}

	// Prevent users from cloning the instance
	public function __clone() {
		throw new Exception('Clone is not allowed.');
	}

	public function getCache($VarName) { return self::$cache->getCache($VarName); }
	public function setCache($VarName, $VarValue, $TimeLimt = Cache::CACHE_FIVE_MINUTES) { return self::$cache->setCache($VarName, $VarValue, $TimeLimt); }
	public function deleteCache($VarName) { return self::$cache->deleteCache($VarName); }
	public function clearCache() { return self::$cache->clear(); }

}
<?php
class controllers_Build {
	static function main($params) {
		$builder_config = array(
			"audioOffset" => (($_POST["youtubeOffset"]['hour'] * (60*60)) + ($_POST["youtubeOffset"]['minute'] * (60)) + ($_POST["youtubeOffset"]['second'])),
			"GIFLoops" => $_POST["GIFLoops"],
			"Duration" => @$_POST["duration"]
		);
		$prep = clipgif_Preparer::getInstance();
		$prep->prepare($_POST['gifURL'] , $_POST['youtubeURL'] , $builder_config );
		echo $prep;
	}

	static function rebuild($params) {
		$config = Config::getInstance();
		$id = Utils::decode($params["id"]);
		$prep = clipgif_Preparer::getInstance( $id );
		if (! $prep) {
			echo "Error rebuilding clip.";
			die();
		}

		$handle = DB::getInstance()->prepare("SELECT id FROM clips WHERE job_id = ?");
		$handle->execute(array($id));
		$row = $handle->fetch();
		$clip_id = $row["id"];

		$handle = DB::getInstance()->prepare("SELECT * FROM jobs WHERE id = ?");
		$handle->execute(array($id));
		if ($handle->rowCount() > 0) {

			$row = $handle->fetch();
			if (@$_SESSION["user_id"] != $row["user_id"]) return false;

			$builder_config = unserialize($row['config']);
			$orig_config = $builder_config;

			if (isset($_POST["youtubeOffset"])) {
				Tags::delete($row['tag_collections_id']);
				if (isset($_POST["tags"])) {
					$collection_id = Tags::save($_POST["tags"]);
				} else {
					$tags = "";
					$collection_id = 0;
				}

                $builder_config["audioOffset"] = (($_POST["youtubeOffset"]['hour'] * (60*60)) + ($_POST["youtubeOffset"]['minute'] * (60)) + ($_POST["youtubeOffset"]['second']));
				$builder_config["GIFLoops"] = $_POST["GIFLoops"];
				$sconfig = serialize($builder_config);
				$update = DB::getInstance()->prepare("UPDATE jobs SET config = :config, tag_collections_id = :tag_collections_id, title = :title WHERE id = :id");
				$update->execute(array(
					':config'=>$sconfig,
					':tag_collections_id'=>$collection_id,
					':title'=>$_POST['title'],
					':id'=>$id
				));
				$update = DB::getInstance()->prepare("UPDATE clips SET title=:title WHERE job_id = :id");
				$update->execute(array(
					':title'=>$_POST["title"],
					//':tags'=>$tags,
					':id'=>$id
				));
			}

			if ($builder_config != $orig_config) { //Do we need to rerender?

				DB::getInstance()->exec("INSERT into deleted (`job_id`,`clip_id`,`when`,`hits`) VALUES (".$id.",".$clip_id.",".time().",0)");
				DB::getInstance()->exec("DELETE FROM clips WHERE job_id = ".$row["id"]);
				if ( ! $prep->prepare($row["source_animate"], $row["source_audio"], $builder_config, true)) {
				}
				echo Scope::getInstance()->
					add("config",$config)->
					render("rebuild.php");
			} else {
				$handle = DB::getInstance()->prepare("SELECT id FROM clips WHERE job_id = ?");
				$handle->execute(array($row['id']));
				if ($handle->rowCount() > 0) {
					$nrow = $handle->fetch();
					header("Location: /".Utils::encode($nrow["id"]));
					die();
				} else {
					echo "Clip not found.";
					die();
				}
			}

		} else {
			echo "Clip not found.";die();
		}
	}

	static function progress($params) {
		$prep = clipgif_Preparer::getInstance( $params['id'] );
		$progress = $prep->getProgress();
		echo json_encode($progress);
	}

	static function delete($params) {
		$config = Config::getInstance();
		$handle = DB::getInstance()->prepare("SELECT user_id FROM clips WHERE id = ?");
		$handle->execute(
			array( Utils::decode( $params["id"] ) )
		);
		if ($res = $handle->fetch()) {
			if (Auth::isAuthed()) {
				if ( Auth::isAdmin() || $res["user_id"] == $_SESSION["user_id"]) {
					$g = glob($res["stored_video"].".*");
					foreach($g as $rm_file) {
						unlink($rm_file);
					}
					$handle = DB::getInstance()->prepare("DELETE from clips WHERE id = ?");
					$handle->execute(
						array( Utils::decode( $params["id"] ) )
					);
				}
			}
		}
		echo Scope::getInstance()->
			add("config",$config)->
			render("deleted.php");
	}

	static function page($params) {
		if (Auth::isAuthed()) {
			$config = Config::getInstance();
			echo Scope::getInstance()->
				add("defaultURL",@$_REQUEST["d"])->
				add("config",$config)->
				render("build.php");
		} else {
			Router::forced404( true );
			return false;
		}
	}

	static function getQ() {
		$handle = DB::getInstance()->query("SELECT count(id) as cnt FROM que");
		$res = $handle->fetch();
		$q = $res['cnt'];
		if (isset($_SESSION["user_id"])) {
			$handle = DB::getInstance()->query("SELECT count(id) as cnt FROM que WHERE user_id=".$_SESSION["user_id"]);
			$res = $handle->fetch();
			$myq = $res['cnt'];
		} else {
			$myq = 0;
		}
		return array('q'=>$q,'myq'=>$myq);
	}

	static function q($params) {
		$d = self::getQ();
		echo json_encode(['q'=>$d['q'],'myq'=>$d['myq']]);
		die();
	}

    static function pageb($params) { //New ClipGif Builder Layout
   		if (Auth::isAuthed()) {
   		$config = Config::getInstance();
   		echo Scope::getInstance()->
   			add("config",$config)->
   			render("build-b.php");
   		} else {
   			Router::forced404( true );
   			return false;
   		}
   	}

    static function animate($params) { //Front end Audio ajax handler
		$config = Config::getInstance();
   		if (Auth::isAuthed()) {
			$gifURL = $params["gifURL"];
			$animate = new clipgif_AnimateGIF( $gifURL );
			$animate->download( $config["work"]["table"] );
			list($frames,$duration) = $animate->getDuration();
			//$spriteFile = $animate->buildSprite( );
			$out = array(
				"frames"=>$frames,
				"duration"=>$duration,
				"sprite"=>$spriteFile
			);

			echo json_encode($out);
   		}
   	}

    static function audio($params) { //Front-end Audio ajax handler
   		if (Auth::isAuthed()) {
   		$config = Config::getInstance();
   		echo Scope::getInstance()->
   			add("config",$config)->
   			render("build.php");
   		} else {
   			Router::forced404( true );
   			return false;
   		}
   	}

}
?>
<?php
	class controllers_Global {

		static function scope() {
			global $config;
			$data = array(
				"meta"=>array(
					"author"=>$config["author"],
					"title"=>$config["title"],
					"description"=>$config["description"]
				)
			);
			$data['user_data'] = clipgif_Auth::user_data();
			return $data;
		}

	}
?>

<?php
	class controllers_Errors {

		static function notFound() {
			header("HTTP/1.0 404 Not Found");
			echo Scope::getInstance()->
			add("request",$_SERVER["REQUEST_URI"])->
			render('404.php');
			die();
		}

	}
?>

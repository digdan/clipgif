<?php
	class controllers_Lists {

		static function front($params) {
			$config = Config::getInstance();
			$params["page"] = 1;
			self::ranked($params,$config["front"]["vote_weight"]);
		}

		static function adspots() {
			$config = Config::getInstance();
			$adspots = array();
			$adspots[] = rand(0,$config["lists"]["posts_per_page"]);
			$adspots[] = rand(0,$config["lists"]["posts_per_page"]); //Twice
			return $adspots;
		}

		static function recent($params=NULL) {
			$config = Config::getInstance();

			$handle = DB::getInstance()->query("SELECT count(id) as cnt from clips");
			$res = $handle->fetch();
			$total = $res["cnt"];

			$page = (isset($params["page"]) ? $params["page"] : 1);
			$paginate = new Pagination($config["lists"]["posts_per_page"],$total,$page,Router::getInstance()->generate('recent')."/");
			$front = $paginate->prePagination();

			$front = $config["lists"]["posts_per_page"] * ($page -1);
			//limit {$front},{$config["lists"]["posts_per_page"]}
			if ($clips = Cache::getInstance()->getCache("lists_recent")) {
				$_SESSION["last_list"] = "list_recent";
			} else {
				$handle = DB::getInstance()->query("
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					clips.*,
					jobs.score as ascore,
					jobs.comments as comments
				FROM clips, user_data, jobs
				WHERE
					clips.user_id = user_data.user_id
				AND
					jobs.id = clips.job_id
				ORDER BY
					`when` DESC
				");

				$clips = array();

				$adspots = self::adspots();

				$adcount = 0;
				while ($res = $handle->fetch()) {
					if (in_array($adcount++,$adspots))
						$clips[] = array('id'=>'ad');

					$clips[] = array(
						"id"=>Utils::encode($res["id"]),
						"data"=>$res
					);
				}

				Cache::getInstance()->setCache("list_recent",$clips,$config["lists"]["cache_time"]);
				$_SESSION["last_list"] = "list_recent";
			}

			$clips = array_slice($clips,$front,$config["lists"]["posts_per_page"]);

			echo Scope::getInstance()->
				add("clips",$clips)->
				add("paginate",$paginate->pagination(true))->
				add("list_type","recent_paged")->
				add("config",$config)->
				add("list_title","Most Recent Clips")->
				render('list.php');
		}

		static function viewed($params) {
			$config = Config::getInstance();

			$handle = DB::getInstance()->query("SELECT count(id) as cnt from clips");
			$res = $handle->fetch();
			$total = $res["cnt"];

			$page = (isset($params["page"]) ? $params["page"] : 1);
			$paginate = new Pagination($config["lists"]["posts_per_page"],$total,$page,Router::getInstance()->generate('top_viewed')."/");
			$front = $paginate->prePagination();

			$front = $config["lists"]["posts_per_page"] * ($page -1);

			if ($clips = Cache::getInstance()->getCache("lists_viewed")) {
				$_SESSION["last_list"] = "list_viewed";
			} else {

				$handle = DB::getInstance()->query("
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					clips.*,
					jobs.score as ascore,
					jobs.comments as comments
				FROM clips, user_data, jobs
				WHERE
					clips.user_id = user_data.user_id
				AND
					jobs.id = clips.job_id
				ORDER BY
					`views` DESC
				");
				$clips = array();

				$adspots = self::adspots();

				$adcount = 0;
				while ($res = $handle->fetch()) {
					if (in_array($adcount++,$adspots))
						$clips[] = array('id'=>'ad');

					$clips[] = array(
						"id"=>Utils::encode($res["id"]),
						"data"=>$res
					);
				}
				Cache::getInstance()->setCache("list_viewed",$clips,$config["lists"]["cache_time"]);
				$_SESSION["last_list"] = "list_viewed";
			}

			$clips = array_slice($clips,$front,$config["lists"]["posts_per_page"]);

			echo Scope::getInstance()->
				add("clips",$clips)->
				add("paginate",$paginate->pagination(TRUE))->
				add("list_type","viewed")->
				add("config",$config)->
				add("list_title","Top Viewed Clips")->
				render('list.php');
		}

		static function ranked($params,$vote_weight=NULL) {
			$config = Config::getInstance();

			$days = $config['ranked']['days'];
			if (is_null($vote_weight)) $vote_weight = $config['ranked']['vote_weight'];

			$handle = DB::getInstance()->query("SELECT count(id) as cnt from clips");
			$res = $handle->fetch();
			$total = $res["cnt"];

			$page = (isset($params["page"]) ? $params["page"] : 1);
			$paginate = new Pagination($config["lists"]["posts_per_page"],$total,$page,Router::getInstance()->generate('top_ranked')."/");
			$front = $paginate->prePagination();

			$day_perc = (1 / $days) * $config['ranked']['age_weight']; //Percentage of score lost per day of age

			//Do we have a cahced value ?
			if ($clips = Cache::getInstance()->getCache("lists_ranked")) {
				$_SESSION["last_list"] = "list_ranked";
			} else {

				$sql = "
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					FLOOR( (UNIX_TIMESTAMP() - jobs.when) / 86400) as age,
					GREATEST( {$days} - (FLOOR((UNIX_TIMESTAMP() - jobs.when) / 86400)) * {$day_perc},0) * 0.1 as modifier,
					clips.*,
					(
						((jobs.score * {$vote_weight}) + clips.views)
						 *
						GREATEST((( {$days} - (FLOOR((UNIX_TIMESTAMP() - jobs.when) / 86400)) * {$day_perc}) * 0.1),0)
					) as sort_score,
					jobs.score as ascore,
					jobs.comments as comments
				FROM clips, user_data, jobs
				WHERE
					clips.user_id = user_data.user_id
				AND
					jobs.id = clips.job_id
				ORDER BY
					`sort_score` DESC, `views` DESC
				";
				$handle = DB::getInstance()->query( $sql );

				$clips = array();
				$adcount = 0;
				$adspots = self::adspots();
				while ($res = $handle->fetch()) {
					if (in_array($adcount++,$adspots))
						$clips[] = array('id'=>'ad');

					$clips[] = array(
						"id"=>Utils::encode($res["id"]),
						"data"=>$res
					);
				}
				Cache::getInstance()->setCache("list_ranked",$clips,$config["lists"]["cache_time"]);
				$_SESSION["last_list"] = "list_ranked";
			}

			$clips = array_slice($clips,$front,$config["lists"]["posts_per_page"]);



			echo Scope::getInstance()->
				add("clips",$clips)->
				add("paginate",$paginate->pagination(TRUE))->
				add("list_type","ranked")->
				add("config",$config)->
				add("list_title","Top Ranked Clips")->
				render('list.php');
		}

		static function user($params) {
			$config = Config::getInstance();

			$user_id = Utils::decode($params["user_id"]);

			$handle = DB::getInstance()->query("SELECT user_data.display_name as user_display_name, users.is_populus FROM users,user_data WHERE users.id = user_data.user_id AND user_data.user_id = {$user_id}");

			if ( ! ($res = $handle->fetch())) {
				header("/");die();
			} else {
				$display_name = $res["user_display_name"];
				$is_populus = $res["is_populus"];
			}

			$handle = DB::getInstance()->query("SELECT count(id) as cnt from clips WHERE user_id = {$user_id}");
			$res = $handle->fetch();
			$total = $res["cnt"];

			$page = (isset($params["page"]) ? $params["page"] : 1);
			$paginate = new Pagination($config["lists"]["posts_per_page"],$total,$page,Router::getInstance()->generate('by_user')."/");
			$front = $paginate->prePagination();

			$front = $config["lists"]["posts_per_page"] * ($page -1);

			if ($clips = Cache::getInstance()->getCache("list_user_".$user_id)) {
				$_SESSION["last_list"] = "list_user_".$user_id;
			} else {

				$handle = DB::getInstance()->query("
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					clips.*,
					jobs.score as ascore
				FROM clips, user_data, jobs
				WHERE
					clips.user_id = user_data.user_id
				AND
					clips.user_id = {$user_id}
				AND
					clips.job_id = jobs.id
				ORDER BY
					`when` DESC
				");
				$clips = array();

				$adspots = self::adspots();

				$adcount = 0;
				while ($res = $handle->fetch()) {
					if (in_array($adcount++,$adspots))
						$clips[] = array('id'=>'ad');

					$clips[] = array(
						"id"=>Utils::encode($res["id"]),
						"data"=>$res
					);
				}
				Cache::getInstance()->setCache("list_user_".$user_id,$clips,$config["lists"]["cache_time"]);
				$_SESSION["last_list"] = "list_user_".$user_id;
			}

			$clips = array_slice($clips,$front,$config["lists"]["posts_per_page"]);

			$s = Scope::getInstance();
			$cu = Scope::globals('user_data');
			if ($cu['is_populus'] && $is_populus) {
				$s->add("list_title",$display_name."'s Latest Clips <a href='/switch/{$user_id}'> switch </a>");
			} else {
				$s->add("list_title",$display_name."'s Latest Clips");
			}
			echo $s->
				add("clips",$clips)->
				add("paginate",$paginate->pagination(TRUE))->
				add("list_type","viewed")->
				add("config",$config)->
				render('list.php');
		}


		static function gifs($params) {
            $config = Config::getInstance();
		    //Gifpool is all submitted gifs
			$xtra = '';
			if (isset($_REQUEST['bpm']) and (is_numeric($_REQUEST['bpm']))) {
				$bpm = $_REQUEST['bpm'];
			} elseif (isset($params['bpm'])) {
				$bpm = $params['bpm'];
			}

			if (isset($bpm)) {
				//Convert BPM to Milliseconds
				$find_dur = 60000 / round( $bpm );
				$fudge = $config["lists"]["bpm_fudge"];

				$durs[] = round($find_dur / 2);
				$durs[] = round($find_dur);
				$durs[] = round($find_dur * 2);
				$durs[] = round($find_dur * 4);
				$durs[] = round($find_dur * 8);
				$durs[] = round($find_dur * 16);
				$durs[] = round($find_dur * 32);


				foreach ($durs as $ms) {
					$ms_low = $ms - $fudge;
					$ms_high = $ms + $fudge;
					$cond[] = "(gifs.duration >= {$ms_low} AND gifs.duration <= {$ms_high})";
				}
				$xtra = "(".join(" OR ", $cond).")";
				Scope::getInstance()->add("bpm",$bpm);
			}

			if ($xtra) {
				$handle = DB::getInstance()->query("SELECT count(id) as cnt from gifs WHERE {$xtra}");
			} else {
				$handle = DB::getInstance()->query("SELECT count(id) as cnt from gifs");
			}

            $res = $handle->fetch();
            $total = $res["cnt"];
			$page = (isset($params["page"]) ? $params["page"] : 1);
			if (isset($bpm)) {
				$paginate = new Pagination($config["lists"]["gifs_per_page"], $total, $page, Router::getInstance()->generate('gifpool_bpm',array('bpm'=>$bpm)) . "/");
			} else {
				$paginate = new Pagination($config["lists"]["gifs_per_page"], $total, $page, Router::getInstance()->generate('gifpool_find') . "/");
			}
			$front = $paginate->prePagination();
			if ($xtra) {
				$handle = DB::getInstance()->query("
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					gifs.*
				FROM gifs, user_data
				WHERE
					gifs.user_id = user_data.user_id
				AND
					{$xtra}
				ORDER BY
					`when` DESC limit {$front},{$config["lists"]["gifs_per_page"]}
				");
			} else {
				$handle = DB::getInstance()->query("
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					gifs.*
				FROM gifs, user_data
				WHERE
					gifs.user_id = user_data.user_id
				ORDER BY
					`when` DESC limit {$front},{$config["lists"]["gifs_per_page"]}
				");
			}

            $gifs = array();
			while ($res = $handle->fetch()) {
				$gifs[$res["id"]] = array(
					"id"=>Utils::encode($res["id"]),
					"data"=>$res
				);
			}

		    echo Scope::getInstance()->
		        add("gifs",$gifs)->
				add("paginate",$paginate->pagination(TRUE))->
				add("bpm",@$bpm)->
				add("list_type","gifpool_page")->
				add("list_title","GIF Pool")->
		        render('gifpool.php');
        }

		static function que($params) {
			$config = Config::getInstance();
			$handle = DB::getInstance()->query("SELECT count(id) as cnt from que");
			$res = $handle->fetch();
			$total = $res["cnt"];

			$page = (isset($params["page"]) ? $params["page"] : 1);
			$page_current = $page;
			$page_total = ceil($total / $config["lists"]["ques_per_page"]);

			$front = $config["lists"]["ques_per_page"] * ($page -1);

			$handle = DB::getInstance()->query("
			SELECT
				user_data.display_name,
				que.id,
				que.job_id,
				que.user_id,
				que.when,
				que.pid,
				jobs.*
			FROM que, user_data, jobs
			WHERE
				que.user_id = user_data.user_id
			AND
				jobs.id = que.job_id
			ORDER BY
				que.`when` ASC  limit {$front},{$config["lists"]["ques_per_page"]}
			");

			$que= array();
			while ($res = $handle->fetch()) {
				$res["job_config"] = unserialize($res["config"]);
				$que[] = $res;
			}

			echo Scope::getInstance()->
				add("que",$que)->
				add("page_total",$page_total)->
				add("list_type","que")->
				add("page_current",$page_current)->
				add("list_title","Clip Que")->
				render('que.php');
		}

		// Function returns the previous/next X clips from last search result
		function proximity( $clip_id = NULL, $span = NULL ) {
			$config = Config::getInstance();
			$list = isset($_SESSION["last_list"]) ? $_SESSION["last_list"] : 'list_recent';
			if (is_null($clip_id)) {
				$clip_id = 0;
			}
			if (is_null($span)) {
				$span = $config["lists"]["proximity"];
			}
			$count = 0;
			if ($clips = Cache::getInstance()->getCache($list)) {
				foreach($clips as $k=>$clip) {
					if ($clip["id"] != "ad") {
						if ($clip["data"]["id"] == $clip_id) {
							for($i=0;$i < $span;$i++) {
								$mark = $k;
							}
						}
					}
				}
				$bottom = ($mark - $span);
				if ($bottom < 0) $bottom = 0;

				foreach(array_slice($clips,$bottom,($span * 2)+1) as $clip) {
					if ($clip_id != $clip["data"]["id"]) $out[] = $clip["id"];
				}
				return $out;
			} else {
				return false;
			}

		}

	}
?>
<?php
class controllers_Register {

	static function main($params) {
		$config = Config::getInstance();
		echo Scope::getInstance()->
			add("config",$config)->
			add("message",'')->
			render('register.php');
	}


    static function verify($params) {
		$id = Utils::decode($params['id']);
		$handle = DB::getInstance()->prepare("SELECT * FROM users WHERE id = :id AND verification_code = :code");
		$handle->execute([':id'=>$id,':code'=>$params['code']]);
		if ($handle->rowCount() > 0) { //Validated
			Auth::getInstance()->verify( $id );
			echo Scope::getInstance()->
				render('register-verified.php');
		} else {
			Router::forced404( TRUE );
			return false;
		}
    }

	static function logout($params) {
		Auth::getInstance()->logout();
		header("Location: /");
		die();
	}

	static function login($params) {
		$out = array();
		$authed = Auth::getInstance()->login( $_POST["email"], $_POST["password"], $_POST["remember"]);

		switch ($authed) {
			case Auth::LOGGED_IN :
					$out["ok"] = true;
				break;
			case Auth::PASSWORD_WRONG :
					$out["error"] = "Password incorrect";
				break;
			case Auth::NOT_VERIFIED :
				$out["error"] = "Email is unverified <A HREF=\"".Router::getInstance()->generate('reverify_email',['email'=>urlencode($_POST['email'])])."\">Resend Verification</A>";
				break;
			case Auth::LOGIN_ERROR :
				$out["error"] = "Error logging in";
				break;
			case Auth::NOT_ACTIVE :
				$out["error"] = "Account is disabled";
				break;
			default :
				$out["error"] = "Account not found";
				break;
		}
		echo json_encode($out);
		die();
	}

	static function reverify($params) {
		$config = Config::getInstance();
		$cg_auth = clipgif_Auth::getInstance();
		$email = urldecode($params['email']);
		if ($cg_auth->reverification( $email )) {
			try {
				$validateLink = Router::getInstance()->generate('verify_email',['id'=>Utils::encode($cg_auth->user_id),'code'=>$cg_auth->verification_code]);
			} catch (Exception $e) {
				var_dump($e);
			}
			$email_template = Scope::getInstance()->
				add('config',$config)->
				add('validation_link',$validateLink)->
				render('email-revalidation.php');

			$mail = Mailer::getInstance( $email , 'Email Revalidation' , $email_template );

			try {
				$mail->setFrom($config['email']['from_address'],$config['email']['from_name'])->send();
			} catch (Exception $e) {
				var_dump($e);
			}

			echo Scope::getInstance()->
				add("config",$config)->
				add("email",$email)->
				add("message",'')->
				render('register-finished.php');
		} else {
			Router::forced404( TRUE );
			return false;
		}
	}

    static function doSwitch($params) {
        session_start();
        $cu = Scope::globals('user_data');
        if ($cu['is_populus']) {
		unset($_COOKIE['remember']);
		session_destroy();
		session_start();
		$_SESSION["user_id"] = $params['user_id'];
	}
	header("Location:/");
	die();
    }

    static function post($params) {
        $config = Config::getInstance();
        $message = "Unable to create account";
        $response = NULL;
        $validate = Validate::getInstance( $response, $_POST, ['display_name','email','password','password_confirmation'] );
        if ( $validate->
            check('display_name')->
                fancy('Display Name')->
                is('string')->
                required()->
                min_length(4)->
                max_length(36)->
            check('email')->
                fancy('E-mail')->
                required()->
                regex('email')->
            check('password')->
                fancy('Password')->
                required()->
                min_length(4)->
                max_length(36)->
                same('password_confirmation')->
            run() ) {
            $cg_auth = clipgif_Auth::getInstance();

            if ( $cg_auth->taken('display_name',$_POST['display_name'])) {
                $message = "Display name taken";
            } else {

                $created = $cg_auth->create(array(
                    'display_name'=>$_POST["display_name"],
                    'email'=>$_POST['email'],
                    'password'=>$_POST['password'],
                    'is_admin'=>false
                ));


                if ($created !== TRUE ) {
					$message = "Unable to create account";
                } else {

					try {
						$validateLink = Router::getInstance()->generate('verify_email',['id'=>Utils::encode($cg_auth->user_id),'code'=>$cg_auth->verification_code]);
					} catch (Exception $e) {
						var_dump($e);
					}

					$email_template = Scope::getInstance()->
						add('config',$config)->
						add('validation_link',$validateLink)->
						render('email-welcome.php');

					$mail = Mailer::getInstance( $_POST["email"] , 'Welcome to ClipGif' , $email_template );

					try {
						$mail->setFrom($config['email']['from_address'],$config['email']['from_name'])->send();
					} catch (Exception $e) {
						var_dump($e);
					}

					echo Scope::getInstance()->
						add("config",$config)->
						add("email",$_POST['email'])->
						add("message",$message)->
						render('register-finished.php');
					die();
				}
            }
        } else {
            $message = $response['message'][0];
        }

		echo Scope::getInstance()->
			add("config",$config)->
			add("message",$message)->
			render('register.php');
    }

}
?>

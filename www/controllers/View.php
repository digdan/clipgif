<?php
class controllers_View {

	CONST VIDEO=1;
	CONST FLASH=2;
	CONST STILL=3;
	CONST THUMB=4;
	CONST MP4=5;

	static function main($params) {
		$config = Config::getInstance();
		$vote = array();
		if (is_numeric($params["id"])) {
			$orig_id = $params["id"];
			$video_id = Utils::encode( $params["id"] );
			$info = self::info( $params["id"] );

			$dimensions = unserialize( $info["dimensions"] );
			$user = array(
				'display_name'=>$info['user_display_name'],
				'id'=>Utils::encode($info['user_id'])
			);
			list($width,$height) = $dimensions;

			if ($width < $config["player"]["min_width"]) {
				$ratio = $config["player"]["min_width"] / $width;
				$width = round($ratio * $width);
				$height = round($ratio * $height);
			}

			$job = clipgif_jobs::load( $info['job_id'] );
			if (isset($_SESSION["user_id"])) {
				$vote = clipgif_Vote::loadFromVote( $info['job_id'], $_SESSION["user_id"]);
			} else {
				$vote = 0;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				if (isset($_POST["comment"])) {
					clipgif_Comments::add($info['job_id'],$_POST['comment']);
				}
			}

			$handle = DB::getInstance()->query("
				SELECT
					*
				FROM gifs
				WHERE
                    gifs.original_url = '{$job->source_animate}'
				");
			$gif = $handle->fetch();
			$bpms = "";
			foreach(clipgif_BPMer::findBPM( $gif["duration"] ) as $bpm) {
				$bpms .= "<span class='label label-primary'>{$bpm} bpm</span> &nbsp; ";
			}

			//Build facebook compatibility
			$meta = array();
			$meta[] = ['property'=>'og:type','content'=>'movie'];
			//$meta[] = ['property'=>'og:type','content'=>'application/x-shockwave-flash'];
			if (isset($job->title)) {
				$meta[] = ['property'=>'og:title','content'=>$job->title];
			} else {
				$meta[] = ['property'=>'og:title','content'=>'ClipGif '.$video_id];
			}
			$meta[] = ['property'=>'og:description','content'=>''];
			$meta[] = ['property'=>'og:image','content'=>'https://www.clipgif.com/still/'.$video_id.'.jpg'];
			$meta[] = ['property'=>'og:video','content'=>$config["player"]["secure_location"].'?file='. urlencode('https://www.clipgif.com/video/'.$video_id.'.'.$config["work"]["type_flash"]).'&width=470&autostart=true'];
			//$meta[] = ['property'=>'og:video:secure_url','content'=>$config["player"]["secure_location"].'?file='. urlencode('http://www.clipgif.com/video/'.$video_id.'.'.$config["work"]["type"]).'&autostart=true'];
			$meta[] = ['property'=>'og:height','content'=>$height];
			$meta[] = ['property'=>'og:width','content'=>$width];

			echo Scope::getInstance()->
				add("meta",$meta)->
				add("orig_id",$orig_id)->
				add("video_id",$video_id)->
				add("image",'http://www.clipgif.com/still/'.$video_id.'.jpg')->
				add("width", $width)->
				add("height", $height)->
				add("user",$user)->
				add("bpms",$bpms)->
				add("views",$info['views'])->
				add("job_id",$info['job_id'])->
				add("job", $job )->
				add("gif", $gif)->
                add("audioParts",$job->audioParts())->
				add("tags", Tags::load($job->tag_collections_id))->
				add("when", $info['when'])->
				add("proximity",controllers_Lists::proximity($orig_id))->
				add("vote",$vote)->
				add("comments",clipgif_Comments::listAll($info['job_id']))->
				render("view/main.php");
		}
	}

	static function viewCounter( $id ) {
		DB::getInstance()->exec("UPDATE clips SET views = views + 1 WHERE id = {$id}");
	}

	static function info($id) {
		$res = DB::getInstance()->query("
			SELECT
				user_data.display_name as user_display_name,
				user_data.user_id as user_id,
				clips.*
			FROM clips, user_data
			WHERE
				clips.id = {$id}
			AND
				clips.user_id = user_data.user_id
			")->fetch();
		return $res;
	}

	static function unavail($id) {
		$config = Config::getInstance();
		echo Scope::getInstance()->
			add("meta",$config["meta"])->
			render("view/unavail.php");
	}

	static function asset($id,$fieldType) {
		$ext = NULL;
	        $config = Config::getInstance();
		$id = Utils::decode($id);
		if (is_numeric($id)) {
			$fieldname = 'stored_video';
			switch($fieldType) {
				case static::MP4 :
					$fieldname = 'stored_video';
					self::viewCounter( $id );
					$ext = 'mp4';
					break;
				case static::VIDEO :
					$fieldname = 'stored_video';
					self::viewCounter( $id );
					$ext = $config["work"]["type"];
					break;
				case static::FLASH :
					$fieldname = 'stored_video';
					self::viewCounter( $id );
					$ext = $config["work"]["type_flash"];
					break;
				case static::STILL :
					$fieldname = 'stored_still';
					break;
                case static::THUMB :
                    $fieldname = 'stored_still';
                    break;
			}

			$handle = DB::getInstance()->query("SELECT ".$fieldname." FROM clips WHERE id = {$id}");
			$stored = $handle->fetch();
			$file_parts = pathinfo($stored[0]);
			$file_path = $stored[0];
			$file_thumb = "thumb-{$file_parts["basename"]}";

			if (! is_null($ext)) $file_path .= ".{$ext}";

            if ($fieldType == static::THUMB) {
				if ($thumb = Cache::getInstance()->getCache($file_thumb)) { //Thumb Cached?
					header('Content-Type: image/jpeg');
					header('Content-Length: ' . strlen($thumb));
					ob_clean();
					flush();
					echo $thumb;
				} else { //Create  Thumb
					$img = imagecreatefromjpeg( $file_path );
					$width = imagesx( $img );
					$height = imagesy( $img );
					$new_width = $config["player"]["thumbnail"];
					$new_height = floor( $height * ( $config["player"]["thumbnail"] / $width ) );
					$tmp_img = imagecreatetruecolor( $new_width, $new_height );
					imagecopyresized( $tmp_img, $img, 0, 0, 0, 0, $new_width, $new_height, $width, $height );
					header('Content-Type: ' . mime_content_type($file_path));
					ob_clean();
					ob_start();
					imagejpeg($tmp_img, NULL , 100);
					$thumb_data = ob_get_contents();
					$result = Cache::getInstance()->setCache($file_thumb,$thumb_data,Cache::CACHE_ONE_DAY);
					ob_end_flush();
				}
            } else { //All other media
	            ob_end_clean();
	            header('Cache-Control: must-revalidate');
	            header('Pragma: public');
                header('Content-Type: ' . mime_content_type($file_path));
                header('Content-Length: ' . filesize($file_path));
                readfile($file_path);
	            flush();
                die();
            }
			exit;
		}
		return false;
	}

	static function video( $params ) {
		self::asset( $params["id"] , static::VIDEO );
	}

	static function flash( $params ) {
		self::asset( $params["id"] , static::FLASH );
	}

	static function mp4( $params ) {
		self::asset( $params["id"] , static::MP4 );
	}

	static function still( $params ) {
		header("Cache-Control: max-age=86400, must-revalidate");
		header("Expires: Sat, 26 Jul 2020 05:00:00 GMT");
		header("Pragma: cache");
		self::asset( $params["id"] , static::STILL );
	}

    static function thumb( $params ) {
	    header("Cache-Control: max-age=86400, must-revalidate");
	    header("Expires: Sat, 26 Jul 2020 05:00:00 GMT");
	    header("Pragma: cache");
	    self::asset( $params["id"] , static::THUMB );
    }

	static function vote( $params ) {
		$job_id = Utils::decode($params["job_id"]);
		$user_id = Utils::decode($params["user_id"]);
		$score = clipgif_Vote::vote($job_id,$_REQUEST["vote"]);
		if ($score == 0) $class = 'neutral';
		if ($score > 0) $class = 'positive';
		if ($score < 0) $class = 'negative';
		echo json_encode(array('score'=>$score,'class'=>$class));
		die();
	}

	static function forum($params) {
		$config = Config::getInstance();
		echo Scope::getInstance()->
			add("meta",$config["meta"])->
			render("forum.php");
	}

}
?>

<?php
	class controllers_GIFPool {

		function findbyBPM($params) { //TODO
			$find_dur = 60000 / $bpm;
			$out = array();
			$sql = "SELECT id,ABS( ? - duration) as nearest FROM gifs ORDER by nearest ASC";
			$find = DB::getInstance()->prepare($sql);
			$find->execute(array($find_dur));
			if ($find->rowCount() > 0) {
				while($res = $find->fetch()) {
					$out[] = $res["id"];
				}
			} else {
				return $out;
			}
		}

		function gifify($params) {
			//if (! isset($_SESSION["user_id"])) die();
			$out = array();
			$url = trim($_REQUEST["url"]);
			$out["url"] = $url;
			$config = Config::getInstance();
			$a = new clipgif_AnimateGIF();

			$check = DB::getInstance()->prepare("SELECT id,duration,frames,height FROM gifs WHERE original_url = ?");
			$check->execute(array( $url ));

            $force_download = false;

			if (($check->rowCount() > 0) and ($force_download===false)) { //Already exists
				$res = $check->fetch();
                $id = $res['id'];
				$update = DB::getInstance()->prepare("UPDATE gifs SET used = used + 1 WHERE original_url = ?");
				$update->execute(array( $url ));

				$duration = array(
					0=>$res['frames'],
					1=>($res['duration'] / 1000)
				);
                $height = $res['height'];

			} elseif ($a->validate($url)) {
				$out["url_valid"] = true;
				$a->setURL( $url );
				$a->download($config["work"]["table"]);
				list($width,$height,$type,$attr) = $a->getSize();

				$duration = $a->getDuration();

				//Save to GIFPool
				$save = DB::getInstance()->prepare("INSERT into gifs (
					`original_url`,
					`filesource`,
					`duration`,
					`frames`,
					`user_id`,
					`used`,
					`height`,
					`width`,
					`active`,
					`when`
				) VALUES (
					:original_url,
					:filesource,
					:duration,
					:frames,
					:user_id,
					:used,
					:height,
					:width,
					:active,
					:when
				) ON DUPLICATE KEY UPDATE used=used+1");

				$g = $save->execute(
					array(
						":original_url"=>$url,
						":filesource"=>"",
						":duration"=>($duration[1]*1000),
						":frames"=>$duration[0],
						":user_id"=>(isset($_SESSION['user_id']) ? $_SESSION["user_id"] : 0),
						":used"=>0,
						":height"=>$height,
						":width"=>$width,
						":active"=>1,
						":when"=>time()
					)
				);
                $id = DB::getInstance()->lastInsertId();
			}

			$out["frames"] = $duration[0];
            $out["id"] = Utils::encode($id);
            $out["height"] = $height;
			$out["duration_ms"] = $duration[1] * 1000;
			$out["bpms"] = "<ul class='list-inline text-center bpms'>";
			$out["bpms"] .= "<li>";
			foreach(clipgif_BPMer::findBPM( $out["duration_ms"] ) as $bpm) {
				$out["bpms"] .= "<span class='label label-primary'>{$bpm} bpm</span> &nbsp; ";
                if (($bpm > 100) and ($bpm < 200)) {
                    $search_bpm = round($bpm);
                }
			}
			$out["bpms"] .= "</li>";
			$out["bpms"] .= "<li><span class='label label-info'> {$out["duration_ms"]} ms</label></li>";
			$out["bpms"] .= "<li><span class='label label-default'> {$out["frames"]} frames</label></li>";
			$out["bpms"] .= "</ul>";
            $out["bpms"] .= "<A HREF='http://jog.fm/workout-songs/at/{$search_bpm}/bpm' TARGET='_BLANK'> Jog.fm </A> &mdash;";
			$out["bpms"] .= "<A HREF='http://cs.ubc.ca/~davet/music/bpm/{$search_bpm}.html' TARGET='_BLANK'> Dave Tompkins Music DB </A>";
			//Save to GIFPool
			echo json_encode($out);

		}


        function single($params) {
            $orig_id = Utils::decode($params["id"]);
            $handle = DB::getInstance()->query("
				SELECT
					user_data.display_name as user_display_name,
					user_data.user_id as user_id,
					gifs.*
				FROM gifs, user_data
				WHERE
					gifs.user_id = user_data.user_id
                AND
                    gifs.id = {$orig_id}
				");
            $gif = $handle->fetch();
            echo Scope::getInstance()->
                add("gif",$gif)->
                render("view/single.php");
        }
	}
?>
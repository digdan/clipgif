<?php
/**
 * Config File to be used with Config Singleton
 *
 * Makes for highly portable configuration file
 *
 */

	$config = array();

	$config["siteKey"] = "AWmWPgsqhhoNV9Aj4iHoFTTeWbYfs4WEgvjU4t3oV4YYaNFWvwrdEUouuFieFJmo";

	$config["title"] = "ClipGif is an application to create short videos based on various content found on the Internet";

	$config["url"] = "http://www.clipgif.com";

	$config["meta"] = array();

	$config["meta"][] = array(
		"charset"=>"utf-8"
	);

	$config["meta"][] = array(
		"name"=>"viewport",
		"content"=>"width=device-width, initial-scale=1.0"
	);

	$config["meta"]["author"] = "Danjelo Morgaux";
	$config["meta"]["description"] = "Mashup of Animated GIFs + Sound = ClipGIF";

	$config["paths"]["templates"] = __DIR__ . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR;

	$config["cache"]["type"] = "memcache";
	$config["cache"]["host"] = "127.0.0.1:11211";

	$config["lists"]["posts_per_page"] = 56;
	$config["lists"]["ques_per_page"] = 16;
	$config["lists"]["gifs_per_page"] = 4;
	$config["lists"]["proximity"] = 3;
	$config["lists"]["cache_time"] = 60 * 60;

	$config["lists"]["bpm_fudge"] = 10; //How many MS we can fudge our searches

	$config["db"]["dsn"] = "mysql:host=localhost;dbname=clipgif";
	$config["db"]["username"] = "clipgif";
	$config["db"]["password"] = "sEYWDqrKMW7TMGHS";

	$config["bot"]["signature"] = "ClipGif Bot v0.2";

	$config["session"]["lifetime"] = (3600 * 24 * 14);
	$config["session"]["domain"] = ".clipgif.com";

    $config["email"]["from_address"] = "noreply@clipgif.com";
    $config["email"]["from_name"] = "ClipGIF";

	/*
	 * Weights are calculated by this calculation
	 *      ((days - (clip_days_old)) * age_weight) + (votes * vote_weight) + views
	 *
	 * 100 views = 1 vote
	 * brand new = 100 points
	 */
	$config["ranked"]["days"] = 12; //Total days of "new" bonus
	$config["ranked"]["age_weight"] = 10; //Weight of days effectiveness
	$config["ranked"]["vote_weight"] = 100;

	$config["player"]["location"] = "http://www.clipgif.com/assets/js/player.swf";
	$config["player"]["secure_location"] = "https://www.clipgif.com/assets/js/player.swf";
	$config["player"]["min_width"] = 480;
    $config["player"]["thumbnail"] = 262;

	$config["work"]["directory"] = dirname(__DIR__).DIRECTORY_SEPARATOR."work";
	$config["work"]["script"] = $config["work"]["directory"].DIRECTORY_SEPARATOR."build.php";
	$config["work"]["table"] = $config["work"]["directory"].DIRECTORY_SEPARATOR."table";
	$config["work"]["storage"] = $config["work"]["directory"].DIRECTORY_SEPARATOR."storage";
	$config["work"]["refresh_rate"] = 500;
	$config["work"]["duration_max"] = 120; //in seconds
	$config["work"]["loop_max"] = 20; //in seconds
	$config["work"]["bitrate"] = "1024k";
	$config["work"]["type"] = "webm";
	$config["work"]["type_flash"] = "flv";

	return $config;
?>

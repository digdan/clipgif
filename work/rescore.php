<?php
	set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__DIR__) . DIRECTORY_SEPARATOR . "www/libs" );

	include(dirname(__DIR__)."/www/autoload.php");

	Config::setFile(dirname(__DIR__).'/www/config.php');
	$config = Config::getInstance();
	DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );


	$all = DB::getInstance()->query("SELECT id from jobs WHERE id > 0 order by id ASC ");

	while($job = $all->fetch()) {
		$job_id = $job["id"];
		echo "RESCORING JOB #{$job_id} : ";
		echo clipgif_Vote::recalcClip($job_id);
		echo "\n";
	}

?>
#!/usr/bin/php -q
<?php
	function buildLog($str) {
		$fp = fopen(dirname(__DIR__)."/log.txt","a");
		fputs($fp,"*** ".$str."\n");
		fclose($fp);
	}

	set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__DIR__) . DIRECTORY_SEPARATOR . "www/libs" );
	include(dirname(__DIR__)."/www/autoload.php");
	Config::setFile(dirname(__DIR__).'/www/config.php');
	$config = Config::getInstance();
	DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );
	$job_id = $argv[1];
	buildLog("{$job_id} : Initating Builder");
	$b = new clipgif_Builder( $job_id );
	$b->getAnimate( $config["work"]["table"] );
	buildLog("{$job_id} : GIF Completed");
	$b->getAudio( $config["work"]["table"] );
	buildLog("{$job_id} : Audio Completed");
	$b->mash( $config );
	buildLog("{$job_id} : MASHING completed");
	$opt = $b->transport( $config["work"]["storage"] );
	buildLog("{$job_id} : Transport Done");
	$b->save( $opt );
	buildLog("{$job_id} : ClipGif Saved");
	//$b->cleanup( $config );
	buildLog("{$job_id} : Cleaned & Complete");
?>

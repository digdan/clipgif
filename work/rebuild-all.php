<?php
	set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__DIR__) . DIRECTORY_SEPARATOR . "www/libs" );

	include(dirname(__DIR__)."/www/autoload.php");

	Config::setFile(dirname(__DIR__).'/www/config.php');
	$config = Config::getInstance();
	DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );


	$all = DB::getInstance()->query("SELECT id from jobs WHERE id > 0 order by id ASC ");

	while($job = $all->fetch()) {
		$job_id = $job["id"];
		echo "BUILDING #### {$job["id"]} ####\n";
		$b = new clipgif_Builder( $job_id );

		$b->getAnimate( $config["work"]["table"] );
		$b->getAudio( $config["work"]["table"] );

		$b->mash( $config );

		$opt = $b->transport( $config["work"]["storage"] );

		$b->save( $opt );

		$b->cleanup( $config );

		unset($b);
	}

?>

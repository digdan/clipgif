<?php
	set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__DIR__) . DIRECTORY_SEPARATOR . "www/libs" );

	include(dirname(__DIR__)."/www/autoload.php");

	Config::setFile(dirname(__DIR__).'/www/config.php');
	$config = Config::getInstance();
	DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );

        $cg_auth = clipgif_Auth::getInstance();

	$loops = 50;
	for($i=0;$i<$loops;$i++) {
	$message = false;
	
	$info = json_decode(file_get_contents("http://api.randomuser.me/"));
	$user = reset($info->results)->user;

	$display_name = $user->username;
	$email = $user->email;
	$password = $user->password.rand(0,99);
	$is_admin = false;
	$is_populus = true;

        if ( $cg_auth->taken('display_name',$display_name)) {
		$message = "Display name taken.\n";
	} else {
		$created = $cg_auth->create(array(
			'display_name'=>$display_name,
			'email'=>$email,
			'password'=>$password,
			'is_admin'=>$is_admin,
			'is_populus'=>$is_populus,
			'is_verified'=>true,
			'is_active'=>true
		));
	}
	if ($message) die($message);
	echo ($created?$display_name." created.\n":"Unable to create account.\n");
	}	

?>

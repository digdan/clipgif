#!/usr/bin/php -q
<?php
set_include_path( get_include_path() . PATH_SEPARATOR . dirname(__DIR__) . DIRECTORY_SEPARATOR . "www/libs" );

include(dirname(__DIR__)."/www/autoload.php");

Config::setFile(dirname(__DIR__).'/www/config.php');
$config = Config::getInstance();
DB::set( $config["db"]["dsn"], $config["db"]["username"], $config["db"]["password"] );

$f = glob($config["work"]["storage"].DIRECTORY_SEPARATOR."*");
$delCount = 0;
foreach($f as $file) {
	$pi = pathinfo($file);
	$res = DB::getInstance()->query("SELECT count(id) as cnt FROM clips WHERE stored_video LIKE '%{$pi["filename"]}%'")->fetch();
	if ($res["cnt"] == 0) {
		unlink($file);
		$delCount++;
	}
}

echo "Removed {$delCount} files.\n ";

?>